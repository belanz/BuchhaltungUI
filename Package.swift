// swift-tools-version: 6.0

import PackageDescription

let package = Package(
	name: "BuchhaltungUI",
	defaultLocalization: "de",
	platforms: [.iOS(.v18), .macOS(.v15)],
	products: [
		.library(
			name: "BuchhaltungUI",
			targets: ["BuchhaltungUI"]),
	],
	dependencies: [
		.package(url: "https://gitlab.com/belanz/Buchhaltung", from: "0.24.0"),
	],
	targets: [
		.target(
			name: "BuchhaltungUI",
			dependencies: [
				"Buchhaltung",
			],
			resources: [.process("Resources")],
			swiftSettings: [
				.define("DEBUG", .when(configuration: .debug))
			]
		),
		.testTarget(
			name: "BuchhaltungUITests",
			dependencies: ["BuchhaltungUI"]),
	]
)
