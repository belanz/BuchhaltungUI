// Copyright 2023-2025 Kai Oezer

import SwiftUI
import DEGAAP
import Buchhaltung
import IssueCollection

public class ObservableDEGAAPEnvironment : ObservableObject
{
	public let degaap : DEGAAPEnvironment

	public init(degaap : DEGAAPEnvironment)
	{
		self.degaap = degaap
	}
}

public class ObservableEmbeddableIssueCollection : ObservableObject
{
	@Published public var embeddableIssues : EmbeddableIssueCollection

	public init(issues : EmbeddableIssueCollection)
	{
		self.embeddableIssues = issues
	}

	public func clear()
	{
		embeddableIssues.clear()
	}

	public var isEmpty : Bool
	{
		embeddableIssues.isEmpty
	}

	public func append(_ newIssues : IssueCollection)
	{
		embeddableIssues.append(newIssues)
	}
}

extension EnvironmentValues
{
	@Entry public var degaapMapping : DEGAAPMappingDescriptor? = DEGAAPMappingDescriptor(source: .default, target: .init(taxonomy: .v6_6))

	@Entry public var recordEntriesAreBalanced : Bool = false

	@Entry public var bhErrorColor : Color = .red

	@Entry public var bhFocusedColor : Color = .blue

	@Entry public var bhHighlightBackgroundColor : Color = .yellow
}
