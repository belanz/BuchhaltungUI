// Copyright 2020-2025 Kai Oezer

import SwiftUI
import Buchhaltung

struct PeriodEntryField : View
{
	@Binding var period : Period
	@State var validationIssueMessage : String = ""
	@Environment(\.bhErrorColor) var errorColor : Color

	var body : some View
	{
	#if os(macOS)
		let spacing = 10
	#else
		let spacing = 20
	#endif
		VStack {
			HStack(spacing: CGFloat(spacing))
			{
				_titledDateEntryField("period.begin", $period.start)
				_titledDateEntryField("period.end", $period.end)
				Spacer()
			}
			.frame(maxWidth: .infinity)
			if !validationIssueMessage.isEmpty {
				TextEditor(text: $validationIssueMessage)
					.scrollContentBackground(.hidden)
					.foregroundColor(errorColor)
					.disabled(true)
				#if os(macOS)
					.frame(height: 28)
				#endif
					.frame(maxWidth: .infinity, alignment: .leading)
			}
		}
		.onAppear{ _validate() }
		.onChange(of: period.start){ _, _ in _validate() }
		.onChange(of: period.end){ _, _ in _validate() }
	}

	@ViewBuilder
	func _titledDateEntryField(_ title : LocalizedStringKey, _ day : Binding<Day>) -> some View
	{
	#if os(macOS)
		let spacing = 4
		let alignment = VerticalAlignment.firstTextBaseline
	#else
		let spacing = 20
		let alignment = VerticalAlignment.center
	#endif
		HStack(alignment: alignment, spacing: CGFloat(spacing))
		{
			txt(title)
			DayEntryField(day: day)
		}
	}

	func _validate()
	{
		validationIssueMessage = period.validationIssue?.description ?? ""
	}
}

extension Period.PeriodIssue : @retroactive CustomStringConvertible
{
	public var description : String
	{
		switch self
		{
			case .endPrecedesStart : return /TR("period.error.order")
			case .periodExceedsOneYear : return /TR("period.error.length")
		}
	}
}

// MARK: - Preview

#Preview("PeriodEntryField", traits: .fixedLayout(width: 400, height: 300))
{
	@Previewable @State var period1 : Period = Period(start: Day(2019, 4, 20)!, end: Day(2019, 12, 31)!)
	@Previewable @State var period2 : Period = Period(start: Day(2020, 4, 20)!, end: Day(2019, 12, 31)!)
	@Previewable @State var period3 : Period = Period(start: Day(2019, 4, 20)!, end: Day(2020, 12, 31)!)

	VStack(spacing: 40) {
		PeriodEntryField(period: $period1)
		PeriodEntryField(period: $period2)
		PeriodEntryField(period: $period3)
	}
	.padding(40)
	.environment(\.bhErrorColor, Color.orange)
}
