// Copyright 2023-2025 Kai Oezer

import SwiftUI
import Buchhaltung

struct PeriodEditView : View
{
	@Binding var period : Period
	@State var showsPeriodEntry : Bool = false
	@Environment(\.bhErrorColor) var errorColor : Color
	@Environment(\.bhHighlightBackgroundColor) var backgroundColor : Color
	@Environment(\.isEnabled) var isEnabled : Bool
	let disabledColor = Color(white: 0, opacity: 0.4)

	var body : some View
	{
		Text(String(period.year))
			.foregroundColor(period.validationIssue != nil ? errorColor : (isEnabled ? .accentColor : disabledColor))
			.frame(maxWidth: .infinity, alignment: .leading)
			.font(.system(size: 18))
			.bold()
			.frame(width: 52)
			.popover(isPresented: $showsPeriodEntry, arrowEdge: .trailing) {
				PeriodEntryField(period: $period)
				#if os(macOS)
					.padding([.leading], 12)
					.padding([.top, .bottom, .trailing], 8)
					.background(backgroundColor)
				#else
					.padding([.leading, .trailing], 16)
					.padding([.top, .bottom], 8)
				#endif
			}
			.onTapGesture {
				showsPeriodEntry.toggle()
			}
	}
}

// MARK: - Preview

#Preview("PeriodEditView")
{
	@Previewable @State var period : Period = .year(2025)!

	PeriodEditView(period: $period)
		.padding()
		.environment(\.bhHighlightBackgroundColor, Color(red: 1.0, green: 1.0, blue: 0.94))
}
