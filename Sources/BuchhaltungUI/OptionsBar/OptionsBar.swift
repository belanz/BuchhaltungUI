// Copyright 2023-2025 Kai Oezer

import SwiftUI
import Buchhaltung
import DEGAAP

struct OptionsBar : View
{
	@Binding var ledger : Ledger

	var body : some View
	{
		HStack
		{
			_periodEditor
		}
		.padding(0)
	}

	private var _periodEditor : some View
	{
		PeriodEditView(period: $ledger.period)
			.help(txt("options.period.help"))
	}
}

// MARK: - Preview

#Preview("OptionsBar", traits: .fixedLayout(width: 400, height: 100))
{
	@Previewable @State var ledger : Ledger = Ledger(period: .year(2021)!, company: Company())

	OptionsBar(ledger: $ledger)
		.padding(20)
}
