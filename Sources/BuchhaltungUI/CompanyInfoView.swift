// Copyright 2020-2025 Kai Oezer

import SwiftUI
import Buchhaltung

struct CompanyInfoView : View
{
	@Binding var company : Company
	let baseReport : Report?
	@Environment(\.bhErrorColor) private var errorColor : Color
	@FocusState private var focusedControl : String?
	@State private var showsBaseReportPrompt : Bool = false

	private static let _emptyCompany = Company()
	private static let _emptyCompanyWithInvisibleName : Company = {
		var tmp = Company()
		tmp.name = [""]
		return tmp
	}()

	var body : some View
	{
		BHSectionContainer
		{
			_baseDataSection
			_seatSection
			_tradeRegistrationSection
			_taxRegistrationSection
			_shareholdersSection
			_ceoSection
			_employeesSection
			_liabilitiesSection
		}
		.valueTrackingOverlayButton(
			value: $company,
			observer: { updatedCompany in
				let baseInfo = baseReport?.notesBelowBalanceSheet.companyInformation
				showsBaseReportPrompt = (baseInfo != Self._emptyCompany)
					&& (baseInfo != Self._emptyCompanyWithInvisibleName)
					&& ((updatedCompany == Self._emptyCompany) || (updatedCompany == Self._emptyCompanyWithInvisibleName))
			},
			isVisible: showsBaseReportPrompt && baseReport != nil,
			text: txt("base_values.apply"),
			topMargin: 12,
			trailingMargin: 20
		) {
			if let baseReport {
				company = baseReport.notesBelowBalanceSheet.companyInformation
			}
		}
	}

	private var _baseDataSection : some View
	{
		BHSubsection("company.main.section_title")
		{
			TextField(/TR("company.name"), text: _companyName)
			TextField(/TR("company.activity"), text: _companyStringBinding(\.business))
			Picker(/TR("company.legal_form"), selection: $company.legalForm)
			{
				txt("company.legal_form.gmbh").tag(Company.LegalForm.gmbh)
				txt("company.legal_form.ug").tag(Company.LegalForm.ug)
			}
			bhSectionMoneyField($company.subscribedCapital, id: "company.capital", focus: $focusedControl)

			Toggle(/TR("company.liquidation"), isOn: Binding<Bool>(
				get : { company.inLiquidation },
				set : { company.inLiquidation = $0 }
			)).toggleStyle(.switch)
		}
	}

	private var _seatSection : some View
	{
		BHSubsection("company.seat.section_title")
		{
			TextField(/TR("company.seat.street"), text: _companyStringBinding(\.seat.street))
			TextField(/TR("company.seat.houseno"), text: _companyStringBinding(\.seat.houseNo))
			TextField(/TR("company.seat.zip"), text: _companyStringBinding(\.seat.zipCode))
			TextField(/TR("company.seat.city"), text: _companyStringBinding(\.seat.cityOrRegion))
			TextField(/TR("company.seat.country"), text: _companyStringBinding(\.seat.country))
		}
	}

	private var _tradeRegistrationSection : some View
	{
		BHSubsection("company.trade.section_title")
		{
			TextField(/TR("company.trade.court"), text: _companyStringBinding(\.tradeRegistration.tradeCourt))
			TextField(/TR("company.trade.id"), text: _companyStringBinding(\.tradeRegistration.tradeRegistrationID))
		}
	}

	private var _taxRegistrationSection : some View
	{
		BHSubsection("company.tax.section_title")
		{
			TextField(/TR("company.tax.st13"), text: _companyStringBinding(\.fiscalRegistration.st13))
				.help(txt("company.tax.st13.help"))

			TextField(/TR("company.tax.bf4"), text: _companyStringBinding(\.fiscalRegistration.bf4))
				.help(txt("company.tax.bf4.help"))
		}
	}

	private var _shareholdersSection : some View
	{
		BHSubsection("company.shareholders.section_title")
		{
			ForEach($company.shareholders) { shareholder in
				let shareholderName = shareholder.wrappedValue.name.isEmpty ? /TR("company.shareholders.placeholder_name \(shareholder.wrappedValue.id)") : shareholder.wrappedValue.name
				let shortTitle = shareholder.wrappedValue.personFirstName.isEmpty ? shareholderName : "\(shareholder.wrappedValue.personFirstName) \(shareholderName)"
				let title = shortTitle + " \(_percentageHolding(of: shareholder.wrappedValue))"
				BHSectionItemView(
					title: title,
					expandedTitle: shortTitle,
					itemDescription: shortTitle,
					content: { _shareholderView(shareholder) },
					removeAction: { company.shareholders.removeAll{ $0.id == shareholder.id } },
					requiresAttention: shareholder.wrappedValue.isMissingEntries || !_shareholderShareValidator(),
					focus: $focusedControl
				)
				.id("shrh \(shareholder.wrappedValue.id)")
			}

			bhSectionAdderButton(_addShareholder)
		}
	}

	private var _ceoSection : some View
	{
		BHSubsection("company.ceo.section_title")
		{
			ForEach($company.chiefExecutives) { executive in
				let title = executive.wrappedValue.name.isEmpty ? /TR("company.ceo.placeholder_name \(executive.wrappedValue.id)") : executive.wrappedValue.name
				BHSectionItemView(
					title: title,
					content: { _ceoView(executive) },
					removeAction: { company.chiefExecutives.removeAll{ $0.id == executive.id } },
					requiresAttention: executive.wrappedValue.isMissingEntries,
					focus: $focusedControl
				)
				.id("ceo \(executive.id)")
			}

			bhSectionAdderButton(_addExecutive)
		}
	}

	private var _employeesSection : some View
	{
		BHSubsection("company.employees.section_title")
		{
			LabeledContent(/TR("company.employees.count"))
			{
				HStack(spacing: 8)
				{
					Spacer()
					TextField(value: $company.numberOfEmployees, format: .number, label: {})
						.labelsHidden()
						.multilineTextAlignment(.trailing)
						.frame(width: 30, height: 14)
					Stepper(label: {},
						onIncrement: { company.numberOfEmployees += 1 },
						onDecrement: { if company.numberOfEmployees > 0 { company.numberOfEmployees -= 1 } }
					)
					.labelsHidden()
				#if os(macOS)
					.frame(width: 12)
				#endif
				}
			}
			.help(txt("company.employees.count.help"))
		}
	}

	private var _liabilitiesSection : some View
	{
		BHSubsection("company.liabilities.section_title")
		{
			ForEach($company.contingentLiabilities) { liability in
				BHSectionItemView(
					title: _liabilityDescription(liability.wrappedValue),
					content: { _liabilityView(liability) },
					removeAction: { company.contingentLiabilities.removeAll{ $0.id == liability.id } },
					focus: $focusedControl
				)
				.id("liab \(liability.id)")
			}

			bhSectionAdderButton(_addLiability)
		}
	}

	private var _companyName : Binding<String>
	{
		Binding<String>(
			get: { company.name.first ?? "" },
			set: { company.name = [$0] }
		)
	}

	private func _companyStringBinding(_ keypath : WritableKeyPath<Company, String?>) -> Binding<String>
	{
		Binding(
			get: { company[keyPath: keypath] ?? "" },
			set: { company[keyPath: keypath] = $0 }
		)
	}

	private func _companyStringBinding(_ keypath : WritableKeyPath<Company, String>) -> Binding<String>
	{
		Binding(
			get: { company[keyPath: keypath] },
			set: { company[keyPath: keypath] = $0 }
		)
	}

	private var _companyLegalFormSelection : Binding<Company.LegalForm>
	{
		Binding(
			get: { company.legalForm },
			set: { company.legalForm = $0 }
		)
	}

	private func _companyMoneyBinding(_ keypath : WritableKeyPath<Company, Money>) -> Binding<String>
	{
		Binding(
			get: { company[keyPath: keypath].description },
			set: { company[keyPath: keypath] = Money(Int($0) ?? 0) }
		)
	}

	@ViewBuilder
	private func _shareholderView(_ shareholder : Binding<Company.Shareholder>) -> some View
	{
		let instanceID = "\(shareholder.id)"
		Form
		{
			BHSectionTextField(text: shareholder.name, id: "company.shareholder.name", instanceID: instanceID, focus: $focusedControl)
			BHSectionTextField(text: shareholder.personFirstName, id: "company.shareholder.name.first", instanceID: instanceID, focus: $focusedControl)
			if shareholder.wrappedValue.personFirstName.isEmpty {
				BHSectionTextField(text: shareholder.taxNumber, id: "company.shareholder.taxID", instanceID: instanceID, focus: $focusedControl, isValid: shareholder.wrappedValue.$taxNumber)
			} else {
				BHSectionTextField(text: shareholder.personTaxID, id: "company.shareholder.personTaxID", instanceID: instanceID, focus: $focusedControl, isValid: shareholder.wrappedValue.$personTaxID)
			}

			bhSectionMoneyField(shareholder.guaranteedAmount,
				id: "company.shareholder.share",
				instanceID: instanceID,
				focus: $focusedControl,
				validator: _shareholderShareValidator
			)

			bhSectionDateField(shareholder.entry, id: "company.shareholder.entry_date", instanceID: instanceID, focus: $focusedControl)
			bhSectionDateField(shareholder.exit, id: "company.shareholder.exit_date", instanceID: instanceID, focus: $focusedControl)
		}
		.formStyle(.columns)
	}

	private var _shareholderShareValidator : MoneyAmountValidator {
		MoneyAmountValidator { _ in
			company.shareholders.reduce(0, {$0 + $1.guaranteedAmount}) <= company.subscribedCapital
		}
	}

	@ViewBuilder
	private func _ceoView(_ exec : Binding<Company.Executive>) -> some View
	{
		let instanceID = "\(exec.id)"
		Form
		{
			BHSectionTextField(text: exec.name, id: "company.ceo.name", instanceID: instanceID, focus: $focusedControl, isValid: !exec.wrappedValue.name.isEmpty)
			BHSectionTextField(text: exec.address, id: "company.ceo.address", instanceID: instanceID, focus: $focusedControl, isValid: !exec.wrappedValue.address.isEmpty)
		}
		.formStyle(.columns)
	}

	private let _itemIDStartValue = 0

	private func _addShareholder()
	{
		let maxIDNumber = company.shareholders.reduce(_itemIDStartValue, { max($0, $1.id) })
		let remainingAmount = company.subscribedCapital - company.shareholders.reduce(0, {$0 + $1.guaranteedAmount})
		var shareholder = Company.Shareholder(id: maxIDNumber + 1)
		shareholder.guaranteedAmount = remainingAmount
		company.shareholders.append(shareholder)
	}

	private func _percentageHolding(of shareholder : Company.Shareholder) -> String
	{
		guard company.subscribedCapital > .zero else { return "(? %)" }
		let percentage = (shareholder.guaranteedAmount / company.subscribedCapital).formatted(.percent.precision(.fractionLength(1)))
		return "(\(percentage))"
	}

	private func _addExecutive()
	{
		let maxIDNumber = company.chiefExecutives.reduce(_itemIDStartValue, { max($0, $1.id) })
		let executive = Company.Executive(id: maxIDNumber + 1)
		company.chiefExecutives.append(executive)
	}

	@ViewBuilder
	private func _liabilityView(_ liability : Binding<Company.ContingentLiability>) -> some View
	{
		Form
		{
			bhSectionPicker(liability.type, id: "company.liability.type", textAndTags: [
				("company.liability.type.assoc", .associatedCompanies),
				("company.liability.type.pension", .pension),
				("company.liability.type.other", .other)
			])

			bhSectionMoneyField(liability.amount, id: "company.liability.amount", instanceID: "\(liability.id)", focus: $focusedControl)

			BHSectionTextField(text: Binding<String>(
					get: { liability.wrappedValue.note ?? "" },
					set: { liability.wrappedValue.note = $0 }
				),
				id: "company.liability.note",
				instanceID: "\(liability.id)",
				focus: $focusedControl
			)
		}
		.formStyle(.columns)
	}

	private func _liabilityDescription(_ liability : Company.ContingentLiability) -> String
	{
		let note = liability.note ?? ""
		let title = !note.isEmpty ? note : {
			switch liability.type
			{
				case .associatedCompanies : return /TR("company.liability.type.assoc")
				case .pension : return /TR("company.liability.type.pension")
				case .other : return /TR("company.liability.type.other")
			}
		}()
		return "\(title) (\(liability.amount.bh) €)"
	}

	private func _addLiability()
	{
		let maxIDNumber = company.contingentLiabilities.reduce(_itemIDStartValue, { max($0, $1.id) })
		let liab = Company.ContingentLiability(id: maxIDNumber + 1, type: .other, amount: .zero)
		company.contingentLiabilities.append(liab)
	}

}

// MARK: - Preview

#Preview("CompanyInfoView", traits: .fixedLayout(width: 400, height: 600))
{
	@Previewable @State var company = Company()

	ScrollView {
		CompanyInfoView(company: $company, baseReport: nil)
	}
}
