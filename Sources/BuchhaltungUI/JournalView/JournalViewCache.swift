// Copyright 2023-2025 Kai Oezer

import SwiftUI
import Buchhaltung
import Dispatch
import OSLog

extension JournalView
{
	/// Caches the modified journal items (due to filtering or editing)
	/// to allow faster access compared to using a filtering `Binding`.
	///
	/// Significantly improves scrolling performance of `List`.
	class Cache : ObservableObject, @unchecked Sendable
	{
		private var _displayedItems = [JournalRecord]()
		private var _filteredOutItems = [JournalRecord]()
		private var _allItems = [JournalRecord]()

		@LowerCased
		private var _lastAppliedFilterKey : String

		private var _dispatchQueue = DispatchQueue(label: "Journal Cache")

		init()
		{}

		/// This property is written to
		/// - when the cache is set up (responding to changes in model data),
		/// - when the contents of the selected item changes.
		var displayedItems : [JournalRecord]
		{
			get {
				_displayedItems
			}
			set {
				_dispatchQueue.sync {
					_updateDisplayedItems(newValue)
				}
			}
		}

		/// - parameter items: the original list of journal items to cache
		///
		/// This function should be called each time the journal items change in the model layer.
		/// It is usually called
		/// - when JournalView is appears in the view tree,
		/// - when model data changes due to undo actions,
		/// - or when the model data changes after merging back the cached values.
		func setUp(items : [JournalRecord], filteredBy filter : String? = nil)
		{
			_dispatchQueue.sync {
				if _allItems != items
				{
					_allItems = items
					if let filter {
						_lastAppliedFilterKey = filter
					}
					let (matchingItems, otherItems) = _applySearchFilter(to: _allItems)
					_filteredOutItems = otherItems
					_updateDisplayedItems(matchingItems)
				}
			}
		}

		/// Updates the displayed items based on the given new search key.
		/// - Postcondition: updates ``displayedItems``
		func updateForSearchKeyChange(to newSearchKey : String)
		{
			let signpostID = signposter.makeSignpostID()
			let intervalState = signposter.beginInterval("journal cache search key change", id: signpostID)

			_dispatchQueue.sync {
				guard _lastAppliedFilterKey != newSearchKey else {
					assertionFailure()
					return
				}
				_mergeBack(signpostID: signpostID)
				_lastAppliedFilterKey = newSearchKey
				let (matchingItems, otherItems) = _applySearchFilter(to: _allItems)
				_filteredOutItems = otherItems
				_updateDisplayedItems(matchingItems)
				signposter.endInterval("journal cache search key change", intervalState)
			}
		}

		func mergeBack() -> [JournalRecord]
		{
			_dispatchQueue.sync {
				return _mergeBack()
			}
		}

		/// Groups the displayed items with the given record IDs
		func groupRecords(_ recordIDs : Set<JournalRecord.ID>)
		{
			if let groupedItems = Buchhaltung.groupRecords(withIDs: recordIDs, in: displayedItems) {
				self.displayedItems = groupedItems
			}
		}

		/// Ungroups the displayed items with the given record IDs
		func ungroupRecords(_ recordIDs : Set<JournalRecord.ID>)
		{
			if let ungroupedItems = Buchhaltung.ungroupRecords(withIDs: recordIDs, in: displayedItems) {
				self.displayedItems = ungroupedItems
			}
		}

		func item(withID recordID : JournalRecord.ID) -> JournalRecord?
		{
			for item in displayedItems {
				if let item = item.record(withID: recordID) {
					return item
				}
			}
			return nil
		}

		func bindingForItem(withID recordID : JournalRecord.ID) -> Binding<JournalRecord>?
		{
			var itemExists = false
			for item in displayedItems {
				if item.record(withID: recordID) != nil {
					itemExists = true
					break
				}
			}
			guard itemExists else { return nil }
			return Binding<JournalRecord>(
				get: {
					for item in self.displayedItems {
						if let record = item.record(withID: recordID) {
							return record
						}
					}
					logger.info("Could not find bound JournalRecord. Returning a dummy record.")
					return JournalRecord()
				},
				set: { newValue in
					var newItems = [JournalRecord]()
					var didUpdateRecord = false
					for var item in self.displayedItems {
						if item.updateRecord(withID: recordID, value: newValue) {
							didUpdateRecord = true
						}
						newItems.append(item)
					}
					if didUpdateRecord {
						self.displayedItems = newItems
					}
				}
			)
		}

		func removeItems(withIDs recordIDs : Set<JournalRecord.ID>)
		{
			var didRemoveSomeItems = false
			var newItems = [JournalRecord]()
			for var item in displayedItems {
				if recordIDs.contains(item.id) {
					didRemoveSomeItems = true
				} else {
					if item.remove(records: recordIDs) {
						didRemoveSomeItems = true
					}
					newItems.append(item)
				}
			}
			if didRemoveSomeItems {
				self.displayedItems = newItems
			}
		}

		private func _updateDisplayedItems(_ newItems : [JournalRecord])
		{
			let signpostID = signposter.makeSignpostID()
			let intervalState = signposter.beginInterval("journal cache update displayed items", id: signpostID)

			let newDisplayedItems = newItems.sorted(by: >)
			if _displayedItems != newDisplayedItems {
				objectWillChange.send()
				_displayedItems = newDisplayedItems
			}

			signposter.endInterval("journal cache update displayed items", intervalState)
		}

		/// - Returns: an array of matching records paired with an array of filtered-out records.
		///
		/// If a record with sub-records matches the search key, all the sub-records
		/// will be included in the search result. That is, the record is included
		/// without filtering its sub-records.
		/// If a sub-record matches the search key, all ancestor records are inluded
		/// in the search result.
		private func _applySearchFilter(to records : [JournalRecord]) -> ([JournalRecord], [JournalRecord])
		{
			guard !_lastAppliedFilterKey.isEmpty else { return (records, []) }
			var matchingRecords = [JournalRecord]()
			var filteredOutRecords = [JournalRecord]()
			for record in records {
				if _recursivelyApplySearchFilter(to: record) {
					matchingRecords.append(record)
				} else {
					filteredOutRecords.append(record)
				}
			}
			return (matchingRecords, filteredOutRecords)
		}

		private func _recursivelyApplySearchFilter(to record : JournalRecord) -> Bool
		{
			if isMatching(record: record, searchKey: _lastAppliedFilterKey) {
				return true
			}
			if let subRecords = record.subRecords {
				for subRecord in subRecords {
					if _recursivelyApplySearchFilter(to: subRecord) {
						return true
					}
				}
			}
			return false
		}

		/// Merges the displayed record back into the source records.
		/// - returns: merged records
		@discardableResult
		private func _mergeBack(signpostID : OSSignpostID? = nil) -> [JournalRecord]
		{
			if _lastAppliedFilterKey.isEmpty {
				_allItems = _displayedItems
			} else {
				_allItems = _displayedItems + _filteredOutItems
				if let signpostID {
					signposter.emitEvent("journal cache merge-back with inverse filtering", id: signpostID)
				}
			}
			return _allItems.sorted()
		}

		@inline(__always)
		private func isMatching(record : JournalRecord, searchKey : String) -> Bool
		{
			record.title.lowercased(with: .current).contains(searchKey)
				|| (record.tags.first{ $0.lowercased(with: .current).contains(searchKey) } != nil)
		}
	}
}

@propertyWrapper
struct LowerCased : Equatable
{
	private var _value : String

	init(_ value : String = "")
	{
		_value = value
	}

	var wrappedValue : String
	{
		get { _value }
		set { _value = newValue.lowercased(with: .current) }
	}
}
