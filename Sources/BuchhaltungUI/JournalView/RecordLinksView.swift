// Copyright 2025 Kai Oezer

import Foundation
import SwiftUI
import Buchhaltung

struct RecordLinksView : View
{
	@Binding var record : JournalRecord
	@State var isExpanded : Bool = false
	@State var linkToBeRemoved : UUID? = nil
	@FocusState var focus : UUID?

	var body : some View
	{
		DisclosureGroup(isExpanded: $isExpanded) {
			VStack {
				ForEach($record.links) { link in
					HStack {
						RecordLinkEditor(link: link, focus: $focus)
						BHItemRemoverButton {
							focus = nil // otherwise crashes
							linkToBeRemoved = link.id
						}
					}
				}
				HStack {
					BHItemAdderButton(bordered: false) {
						record.links.append(JournalRecord.Link())
					}
					Spacer()
				}
			}
			.padding(EdgeInsets(top: 4, leading: 12, bottom: 0, trailing: 12))
		} label: {
			Image(systemName: "link")
				.opacity(record.links.isEmpty ? 0.5 : 1.0)
				.help(txt("journal.record.links.help"))
		}
		.modifier(RecordLinkRemovalDialog(links: $record.links, linkToBeRemoved: $linkToBeRemoved))
		.onAppear {
			isExpanded = !record.links.isEmpty
		}
	}
}

struct RecordLinkRemovalDialog : ViewModifier
{
	@Binding var links : [JournalRecord.Link]
	@Binding var linkToBeRemoved : UUID?

	func body(content: Content) -> some View
	{
		content
			.confirmationDialog(txt("journal.record.link.removal.title"), isPresented: _showsConfirmationAlert) {
				Button(role: .destructive) {
					guard let linkToBeRemoved else { return }
					let newLinks = links.filter { $0.id != linkToBeRemoved }
					withAnimation{
						links = newLinks
					}
				} label: {
					txt("journal.record.link.removal.confirm")
				}
				Button(role: .cancel) {
					_showsConfirmationAlert.wrappedValue = false
				} label : {
					txt("journal.record.link.removal.cancel")
				}
			} message: {
				txt("journal.record.link.removal.message")
			}
	}

	private var _showsConfirmationAlert : Binding<Bool> {
		Binding<Bool>(
			get: { linkToBeRemoved != nil },
			set: { if !$0 { self.linkToBeRemoved = nil } }
		)
	}
}

#Preview("RecordLinksView", traits: .fixedLayout(width: 500, height: 200))
{
	@Previewable @State var record : JournalRecord = JournalRecord(
		date: Day(2025, 3, 5)!,
		title: "tax return",
		note: "Umsatzsteuererstattung aus dem Kauf der Hardware für Projekt X",
		links: [JournalRecord.Link(url:"https://en.wikipedia.org/wiki/Project_X")],
		debit: [.init(account: .init("1200")!, value: 500)],
		credit: [.init(account: .init("1540")!, value: 500)]
	)

	VStack {
		RecordLinksView(record: $record)
		Spacer()
	}
	.padding()
}

// MARK: -

struct RecordLinkEditor : View
{
	@Binding var link : JournalRecord.Link
	var focus : FocusState<UUID?>.Binding
	@State private var _isShowingPreview : Bool = false
	@Environment(\.colorScheme) var colorScheme

	var body : some View
	{
		HStack {
			TextField(text: $link.url, prompt: txt("journal.record.link.editor.prompt"), label: {})
				.focused(focus, equals: link.id)
				.font(.body)
				.foregroundStyle(_isValidURL ? .primary : (colorScheme == .dark ? Color(red: 1.0, green: 0.6, blue: 0.6) : Color.red))
			_browseButton
			_previewButton
		}
	}

	var _isValidURL : Bool
	{
		guard let url = URL(string: link.url) else { return false }
		if url.isFileURL {
			var isDirectory : ObjCBool = false
			return FileManager.default.fileExists(atPath: url.path, isDirectory: &isDirectory) && !isDirectory.boolValue
		}
		return url.scheme != nil && url.lastPathComponent.isEmpty == false
	}

	private var _browseButton : some View
	{
		FileBrowseButton { url in
			if let newLink = url {
				link = .init(url: newLink.absoluteString)
			}
		}
	}

	private var _previewButton : some View
	{
		Button {
			if let url = URL(string: link.url) {
#if canImport(AppKit)
				if url.isFileURL {
					NSWorkspace.shared.activateFileViewerSelecting([url])
				} else {
					NSWorkspace.shared.open(url)
				}
#elseif canImport(UIKit)
				if UIApplication.shared.canOpenURL(url) {
					UIApplication.shared.open(url)
				}
#endif
			}
		} label: {
			Image(systemName: "eye")
		}
		.toggleStyle(.button)
		.buttonStyle(.borderless)
		.disabled(!_isValidURL)
	}
}

#Preview("RecordLinkEditor variations")
{
	@Previewable @State var link1 = JournalRecord.Link(url: "file:///Users/johndoe/Documents/invoice.pdf")
	@Previewable @State var link2 = JournalRecord.Link(url: "johndoe/Documents/invoice.pdf")
	@Previewable @State var link3 = JournalRecord.Link(url: "file:///")
	@Previewable @State var link4 = JournalRecord.Link(url: "https://en.wikipedia.org/wiki/Project_X")
	@Previewable @FocusState var focus : UUID?

	VStack {
		RecordLinkEditor(link: $link1, focus: $focus)
		RecordLinkEditor(link: $link2, focus: $focus)
		RecordLinkEditor(link: $link3, focus: $focus)
		RecordLinkEditor(link: $link4, focus: $focus)
	}
	.padding()
}
