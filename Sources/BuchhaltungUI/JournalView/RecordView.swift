// Copyright 2020-2025 Kai Oezer

import SwiftUI
import Buchhaltung
import DEGAAP
import OrderedCollections

struct RecordView : View
{
	@Binding var record : JournalRecord
	var allowedPeriod : Period
	@State var isExpandingNotes : Bool = false

	var body : some View
	{
		ScrollView {
			VStack(alignment: .leading)
			{
				RecordEntriesView(record: $record, allowedPeriod: allowedPeriod)
				Spacer().frame(height: 20)
				_tags
				RecordNotesView(record: $record)
				RecordLinksView(record: $record)
			}
			.padding(.top, 10)
		}
		.scrollContentBackground(.hidden)
		.scrollBounceBehavior(.basedOnSize, axes: [.horizontal])
	}

	private var _tags : some View
	{
		HStack(spacing: 2) {
			Image(systemName: "tag")
				.fixedSize()
				.opacity(0.4)
				.help(txt("journal.record.tags.help"))
			RecordTagsEntryField(tags: $record.tags)
		}
		.frame(height: 36)
		.padding(.leading, 12)
	}
}

#Preview("RecordView")
{
	@Previewable @State var record : JournalRecord = JournalRecord(
		date: Day(2025, 3, 5)!,
		title: "tax return",
		note: "Umsatzsteuererstattung aus dem Kauf der Hardware für Projekt X",
		links: [JournalRecord.Link(url: "https://en.wikipedia.org/wiki/Project_X")],
		debit: [.init(account: .init("1200")!, value: 500)],
		credit: [.init(account: .init("1540")!, value: 500)]
	)

	RecordView(record: $record, allowedPeriod: .init(start: Day(2025,1,1)!, end: Day(2025,12,31)!))
		.padding()
		.environment(\.degaapMapping, DEGAAPMappingDescriptor(source: .skr03, target: DEGAAPChartDescriptor(taxonomy: .v6_5)))
		.environmentObject(ObservableDEGAAPEnvironment(degaap: DEGAAPEnvironment()))
}

// MARK: -

struct RecordEntriesView : View
{
	@Binding var record : JournalRecord
	var allowedPeriod : Period
	@FocusState var focus : String?

	var body : some View
	{
		VStack {
			HStack(spacing: 10) {
				DayEntryField(day: $record.date, period: allowedPeriod)
					.fixedSize()
					.focused($focus, equals: "day")
				TextField(text: $record.title, label: { txt("journal.record.title") })
					.textFieldStyle(.plain)
					.bhUnderlinedInput(focus: $focus, focusID: "title", isValid: !record.title.isEmpty)
			}
			Spacer().frame(height: 10)
			VStack {
				AccountEntriesView(entries: $record.debit, focus: $focus, focusID: "debit")
					.environment(\.accountEntryType, .debit)
				AccountEntriesView(entries: $record.credit, focus: $focus, focusID: "credit")
					.environment(\.accountEntryType, .credit)
			}
			.padding([.leading], 20)
			.environment(\.recordEntriesAreBalanced, record.isBalanced)
		}
		.padding(EdgeInsets(top: 2, leading: 2, bottom: 12, trailing: 2))
		.frame(maxWidth: .infinity)
		.onAppear {
#if false // turned off because it irritates the user when the editor grabs focus from the records table
			if record.title.isEmpty {
				focus = "title"
			}
#endif
		}
	}
}

#Preview("RecordEntriesView variations", traits: .fixedLayout(width: 440, height: 400))
{
	@Previewable @State var record1 : JournalRecord = JournalRecord()
	@Previewable @State var record2 : JournalRecord = JournalRecord(
		date: Day(2022, 3, 5)!,
		title: "tax return",
		debit: [.init(account: .init("1200")!, value: 500)],
		credit: [.init(account: .init("1540")!, value: 500)]
	)
	@Previewable @State var record3 : JournalRecord = JournalRecord(
		date: Day(2022, 3, 5)!,
		title: "tax return",
		debit: [.init(account: .init("1200")!, value: 500)],
		credit: [.init(account: .init("1540")!, value: 400)]
	)

	VStack(spacing: 20) {
		RecordEntriesView(record: $record1, allowedPeriod: Period.year(2022)!)
		RecordEntriesView(record: $record2, allowedPeriod: Period.year(2022)!)
		RecordEntriesView(record: $record3, allowedPeriod: Period.year(2022)!)
		Spacer()
	}
	.padding(20)
	.environment(\.degaapMapping, DEGAAPMappingDescriptor(source: .skr03, target: DEGAAPChartDescriptor(taxonomy: .v6_5)))
	.environmentObject(ObservableDEGAAPEnvironment(degaap: DEGAAPEnvironment()))
}

// MARK: -

struct RecordNotesView : View
{
	@Binding var record : JournalRecord
	@State var isExpanded : Bool = false

	var body : some View
	{
		DisclosureGroup(isExpanded: $isExpanded) {
			TextEditor(text: $record.note)
				.font(.body)
				.padding(4)
				.background(RoundedRectangle(cornerRadius: 6).fill(.background))
				.padding(EdgeInsets(top: 4, leading: 12, bottom: 0, trailing: 12))
		} label: {
			Image(systemName: "text.bubble")
				.opacity(record.note.isEmpty ? 0.5 : 1.0)
				.help(txt("journal.record.note.help"))
		}
		.onAppear {
			isExpanded = !record.note.isEmpty
		}
	}
}
