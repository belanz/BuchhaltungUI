// Copyright 2022-2025 Kai Oezer

import SwiftUI
import Buchhaltung
import OrderedCollections

enum AccountEntryType : String
{
	case debit
	case credit
}

extension EnvironmentValues
{
	@Entry var accountEntryType : AccountEntryType = .credit
}

// MARK: -

struct AccountEntryView : View
{
	@Binding var entry : JournalRecord.Entry?
	var focus : FocusState<String?>.Binding
	var focusID : String
	@Environment(\.accountEntryType) var type : AccountEntryType
	@Environment(\.recordEntriesAreBalanced) var balanced : Bool
	@Environment(\.bhErrorColor) var errorColor : Color

	var body : some View
	{
		HStack(spacing: 8)
		{
			_accountTypeIndicator
			HStack(alignment: .bottom, spacing: 8)
			{
				let placeholder = "journal.record.entry.\(type.rawValue).account_id.help"
				AccountEntryField(account: _entryAccount, placeholder: placeholder, focus: focus, focusID: focusID + ".account")
				_moneyEntryField
			}
		}
	}

	private var _accountTypeIndicator : some View
	{
		txt(type == .debit ? "journal.record.entry.debit.short" : "journal.record.entry.credit.short")
			.foregroundColor(Color(white: 0.8))
			.help(type == .debit ? /TR("journal.record.entry.debit") : /TR("journal.record.entry.credit"))
	}

	private var _moneyEntryField : some View
	{
		TextField(value: _entryValue, format: .bhMoneyEntry, label: {})
			.textFieldStyle(.plain)
			.labelsHidden()
			.bhMoney()
			.bhUnderlinedInput(focus: focus, focusID: focusID + ".value", isValid: balanced)
			.frame(minWidth: 80, idealWidth: 90, maxWidth: 120)
	}

	// TODO: Performance impact analysis (bindings that modify View state properties trigger view updates).
	private var _entryAccount : Binding<String>
	{
		Binding<String>(
			get: { entry?.account.code ?? "" },
			set: { entry?.account = .init($0) ?? .none }
		)
	}

	// TODO: Performance impact analysis (bindings that modify View state properties trigger view updates).
	private var _entryValue : Binding<Money>
	{
		Binding<Money>(get: { entry?.value ?? .zero }, set: { entry?.value = $0 })
	}
}

// MARK: -

struct AccountEntriesView : View
{
	@Binding var entries : OrderedSet< JournalRecord.Entry >
	@Environment(\.accountEntryType) var type : AccountEntryType
	var focus : FocusState<String?>.Binding
	let focusID : String
	private let _buttonColor = Color(white: 0.7)

	var body : some View
	{
		ForEach(0..<entries.count, id: \.self) { index in
			HStack(spacing: 12)
			{
				_addButton(for: index)
				AccountEntryView(entry: _entry(at: index), focus: focus, focusID: focusID+".\(index)")
				_removeButton(for: index)
				#if os(macOS)
					.offset(y: -1)
				#endif
			}
#if os(macOS)
			.focusSection()
#endif
			.bhInsertRemoveItemTransition()
		}
	}

	private func _addButton(for index : Int) -> some View
	{
		HStack
		{
			Spacer()

			if index == entries.count - 1
			{
				BHItemAdderButton(bordered:false, color: _buttonColor) {
					entries.append(.init(account: .none, value: .zero))
				}
			}
		}
		.frame(width: type == .debit ? 20 : 40)
	}

	private func _removeButton(for index : Int) -> some View
	{
		HStack
		{
			if entries.count > 1
			{
				BHItemRemoverButton(bordered: false, color: _buttonColor) {
					entries.remove(at: index)
				}
			}

			Spacer()
		}
		.frame(width: type == .debit ? 40 : 20)
	}

	// TODO: Performance impact analysis (bindings that modify View state properties trigger view updates).
	private func _entry(at index : Int) -> Binding<JournalRecord.Entry?>
	{
		Binding<JournalRecord.Entry?>(
			get: {
				guard entries.count > index else { return nil }
				return entries.elements[index]
			},
			set: {
				if let entry = $0
				{
					entries.remove(at: index)
					entries.insert(entry, at: index)
				}
			}
		)
	}
}

// MARK: - Preview

import DEGAAP

#Preview("AccountEntriesView debit", traits: .fixedLayout(width: 400, height: 200))
{
	@Previewable @State var entries = OrderedSet<JournalRecord.Entry>([.init(account: .init("1200")!, value: 400)])
	@Previewable @FocusState var focus : String?
	@Previewable let degaap = DEGAAPEnvironment()

	VStack {
		AccountEntriesView(entries: $entries, focus: $focus, focusID: "")
			.padding(20)
			.environment(\.accountEntryType, .debit)
			.environmentObject(ObservableDEGAAPEnvironment(degaap: degaap))
		Spacer()
	}
}

#Preview("AccountEntriesView credit", traits: .fixedLayout(width: 400, height: 200))
{
	@Previewable @State var entries = OrderedSet<JournalRecord.Entry>([.init(account: .init("1200")!, value: 400)])
	@Previewable @FocusState var focus : String?
	@Previewable let degaap = DEGAAPEnvironment()

	VStack {
		AccountEntriesView(entries: $entries, focus: $focus, focusID: "")
			.padding(20)
			.environment(\.accountEntryType, .credit)
			.environmentObject(ObservableDEGAAPEnvironment(degaap: degaap))
		Spacer()
	}
}
