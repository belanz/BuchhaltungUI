// Copyright 2022-2025 Kai Oezer

import SwiftUI

struct AnnotationTextField : View
{
	let placeholder : String
	@Binding var annotatedText : AnnotatedText
	var suggestionProvider : AnnotationTextField.SuggestionProvider? = nil
	var focus : FocusState<String?>.Binding
	var focusID : String

	@State var suggestions : [AnnotatedText] = []

	var body : some View
	{
		TextField(text: _displayedText, label: { txt(LocalizedStringKey(placeholder)) })
			.textFieldStyle(.plain)
			.monospaced()
			.disableAutocorrection(true)
			.textInputSuggestions(suggestions) { suggestion in
				Text(suggestion.displayFormatted)
					.textInputCompletion(suggestion.text)
			}
			.onChange(of: annotatedText.text) { oldText, newText in
				suggestions.removeAll()
				// fetching new suggestions if the text exceeds a minimum length
				if newText.count > 1 {
					Task { @MainActor in
						suggestions = await suggestionProvider?.suggest(for: newText) ?? []
					}
				}
			}
			.help(annotatedText.annotation)
	}

	private var _displayedText : Binding<String>
	{
		Binding<String>(
			get: { annotatedText.text },
			set: { annotatedText.text = $0 }
		)
	}
}

// MARK: - Preview

#Preview("AnnotationTextField with suggestions", traits: .fixedLayout(width: 300, height: 40))
{
	@Previewable @State var annotatedText : AnnotationTextField.AnnotatedText = .init("Hello", annotation: "World")
	@Previewable @FocusState var focus : String?
	@Previewable @State var suggestions : [AnnotationTextField.AnnotatedText] = [
		.init("1", annotation: "one"),
		.init("2", annotation: "two"),
		.init("3", annotation: "three")
	]

	AnnotationTextField(placeholder: "placeholder", annotatedText: $annotatedText, focus: $focus, focusID: "", suggestions: suggestions)
		.padding(6)
		.background(RoundedRectangle(cornerSize: CGSize(width: 6, height: 6)).fill(Color.clear).stroke(Color.gray))
		.padding(20)
}
