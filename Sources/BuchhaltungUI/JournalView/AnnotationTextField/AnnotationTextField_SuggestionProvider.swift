// Copyright 2023-2025 Kai Oezer

import DEGAAP

extension AnnotationTextField
{
	struct SuggestionProvider
	{
		let chartMapping : DEGAAPMappingDescriptor
		let degaap : DEGAAPEnvironment

		init?(chartMapping : DEGAAPMappingDescriptor?, degaap : DEGAAPEnvironment)
		{
			guard let chartMapping else {
				logger.info("Chart mapping not set in completion provider closure.")
				return nil
			}
			self.chartMapping = chartMapping
			self.degaap = degaap
		}

		func suggest(for input : String) async -> [AnnotatedText]?
		{
			let chartIDsAndDescriptions = await degaap.sourceChartItems(describedBy: input, mapping: chartMapping, caseSensitive: false)
			guard !chartIDsAndDescriptions.isEmpty else { return nil }
			return chartIDsAndDescriptions.map{ AnnotationTextField.AnnotatedText($0.0.description, annotation: $0.1) }
		}
	}
}
