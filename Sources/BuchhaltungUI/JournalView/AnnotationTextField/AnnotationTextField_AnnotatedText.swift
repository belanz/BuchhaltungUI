//  Copyright 2022-2024 Kai Oezer

import Foundation
import SwiftUI

extension AnnotationTextField
{
	struct AnnotatedText : Equatable, Identifiable, Hashable, CustomStringConvertible, ExpressibleByStringLiteral
	{
		var text : String
		var annotation : String

		init(_ text : String, annotation : String)
		{
			self.text = text
			self.annotation = annotation
		}

		init(_ textAndAnnotation : String)
		{
			self.text = ""
			self.annotation = ""
			if let (_, text, annotation) = textAndAnnotation.wholeMatch(of: /^\s*([A-Za-z0-9_-]+(?:\.[A-Za-z0-9_-]+)*)(?:\s*\((.*)\))?\s*$/)?.output
			{
				self.text = String(text)
				if let annotation
				{
					self.annotation = String(annotation.trimmingCharacters(in: .whitespaces))
				}
			}
		}

		init(stringLiteral input : StringLiteralType)
		{
			self.init(input)
		}

		var id : Int { text.hashValue }

		var description : String { annotation.isEmpty ? text : "\(text) (\(annotation))" }

		var isEmpty : Bool { description.isEmpty }
		
		var displayFormatted : AttributedString
		{
			var result = AttributedString("\(text) ")
			result.font = .body.bold().monospaced()
			result.append(AttributedString(annotation))
			return result
		}

		static let empty = AnnotatedText("", annotation: "")
	}
}
