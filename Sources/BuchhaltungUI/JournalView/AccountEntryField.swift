// Copyright 2022-2025 Kai Oezer

import SwiftUI
import DEGAAP

/// A text field for entering an account ID.
///
/// Supports text completion.
struct AccountEntryField : View
{
	@Binding var account : String
	var placeholder : String?
	@Environment(\.degaapMapping) var chartMapping : DEGAAPMappingDescriptor?
	@EnvironmentObject var degaap : ObservableDEGAAPEnvironment
	var mappedAccountIDAndDescription : (String, String)?
	var focus : FocusState<String?>.Binding
	let focusID : String

	/// the descriptive text for the current account, fetched asynchronously from DEGAAP
	@State var annotation : String = ""

	/// whether the current account is part of the accounts chart
	@State var isValidAccount = true

	var body : some View
	{
		AnnotationTextField(
			placeholder: placeholder ?? "",
			annotatedText: _annotatedAccountText,
			suggestionProvider: .init(chartMapping: chartMapping, degaap: degaap.degaap),
			focus: focus,
			focusID: focusID
		)
		.bhUnderlinedInput(focus: focus, focusID: focusID, isValid: isValidAccount)
		.task {
			await _updateAnnotationAndValidity()
		}
	}

	// TODO: Performance impact analysis (bindings that modify View state properties trigger view updates).
	private var _annotatedAccountText : Binding<AnnotationTextField.AnnotatedText>
	{
		Binding<AnnotationTextField.AnnotatedText>(
			get: {
				AnnotationTextField.AnnotatedText(account, annotation: annotation)
			},
			set: {
				account = $0.text
				Task.detached {
					await _updateAnnotationAndValidity()
				}
			}
		)
	}

	private func _updateAnnotationAndValidity() async
	{
		guard let chartMapping else {
			logger.info("Accessing annotated text without a chart mapping.")
			return
		}

		if let sourceChartItem = DEGAAPSourceChartItemID(account) {
			isValidAccount = await degaap.degaap.chartItem(for: sourceChartItem, mapping: chartMapping) != nil
			annotation = await degaap.degaap.description(for: sourceChartItem, mapping: chartMapping) ?? ""
		} else {
			isValidAccount = false
			annotation = ""
		}
	}
}

// MARK: - Preview

#Preview("AccountEntryField", traits: .fixedLayout(width: 200, height: 30))
{
	@Previewable @State var accountID : String = "Hello"
	@Previewable @FocusState var focus : String?
	@Previewable let degaap = DEGAAPEnvironment()

	AccountEntryField(account: $accountID, focus: $focus, focusID: "")
		.environment(\.degaapMapping, DEGAAPMappingDescriptor(source: .skr03, target: .init(taxonomy: .v6_7, fiscal: true)))
		.environmentObject(ObservableDEGAAPEnvironment(degaap: degaap))
}
