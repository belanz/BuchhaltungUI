// Copyright 2020-2025 Kai Oezer

import SwiftUI
import Buchhaltung
import DEGAAP

struct JournalView : View
{
	@Binding var journal : Journal
	@Binding var taxonomy : DEGAAPTaxonomyVersion
	let businessPeriod : Period

	/// Should be set to `true` during initialization if the setup of records is expected to take longer than 100 ms.
	@State var showingCenterWaitIndicator : Bool = false

	enum FocusArea : Hashable
	{
		case recordTable
		case recordEditor
	}

	@FocusState private var focus : FocusArea?

	@SceneStorage("JournalView.SearchKey") private var searchKey : String = ""
	@State private var chartMapping : DEGAAPMappingDescriptor = .init(source: .default, target: DEGAAPChartDescriptor(taxonomy: .v6_7))
	@State private var showingMappingWaitIndicator : Bool = false
	@State private var selectedRecordIDs : Set<JournalRecord.ID> = []
	@State private var toBeRemovedRecordIDs : Set<JournalRecord.ID> = []
	@State private var showingRemoveConfirmationPrompt : Bool = false
	@State private var inspectorVisible : Bool = true
	@StateObject private var cache = Cache()
	@Environment(\.bhHighlightBackgroundColor) private var highlightBackgroundColor : Color
	@EnvironmentObject var degaap : ObservableDEGAAPEnvironment
	@FocusedObject var providedCommands : BuchhaltungCommands?

	var body : some View
	{
		ZStack
		{
			if cache.displayedItems.isEmpty {
				_emptyJournalView
			}
			_recordsListAndInspector
				.searchable(text: $searchKey)
		}
		.onChange(of: journal.chart) { _, newChart in
			Task {
				chartMapping = await _loadChartMapping(from: newChart, to: taxonomy) ?? .init(source: .default, target: DEGAAPChartDescriptor(taxonomy: .v6_7))
			}
		}
		.onChange(of: taxonomy) { _, newTaxonomy in
			Task {
				chartMapping = await _loadChartMapping(from: journal.chart, to: newTaxonomy) ?? .init(source: .default, target: DEGAAPChartDescriptor(taxonomy: .v6_7))
			}
		}
		.onChange(of: searchKey) { _, newSearchKey in
			selectedRecordIDs.removeAll()
			cache.updateForSearchKeyChange(to: newSearchKey)
		}
		.onChange(of: cache.displayedItems) { _, _ in
			// This can trigger the undo manager to flag the document as dirty
			// even if the records merged back (and their sorting) are the same.
			journal.records = cache.mergeBack()
		}
		.onChange(of: journal) { _, newJournal in
			cache.setUp(items: newJournal.records)
		}
		.onChange(of: selectedRecordIDs) { _, newSelectedRecordIDs in
			guard let providedCommands else { return }
			if !newSelectedRecordIDs.isEmpty {
			providedCommands.duplicateJournalRecord = { _duplicateRecords(newSelectedRecordIDs) }
			} else {
				providedCommands.duplicateJournalRecord = nil
			}
		}
		.onAppear {
			if let providedCommands {
				providedCommands.addJournalRecord = { _addRecord() }
				providedCommands.duplicateJournalRecord = nil
			}
			// The focus is shifted to the table when it appears.
			// Selecting a row would otherwise require two clicks.
			focus = .recordTable
		}
		.onDisappear {
			if let providedCommands {
				providedCommands.addJournalRecord = nil
				providedCommands.duplicateJournalRecord = nil
			}
		}
		.task {
			chartMapping = await _loadChartMapping(from: journal.chart, to: taxonomy) ?? .init(source: .default, target: DEGAAPChartDescriptor(taxonomy: .v6_7))
		}
		.task {
			if !journal.records.isEmpty {
				cache.setUp(items: journal.records, filteredBy: searchKey)
			}
			Task { @MainActor in
				showingCenterWaitIndicator = false
			}
		}
	}

	private var _recordsListAndInspector : some View
	{
		_recordList
			.inspector(isPresented: $inspectorVisible) {
				VStack {
					if selectedRecordIDs.count == 1,
						 let selectedRecordID = selectedRecordIDs.first,
						 let selectedRecordBinding = cache.bindingForItem(withID: selectedRecordID) {
						_recordEditor(selectedRecordBinding)
						Spacer()
						Divider()
						JournalInfoView(journal: journal, selectedRecordID: selectedRecordID)
					} else {
						JournalPropertiesView(journal: $journal)
						Spacer()
						Divider()
						JournalInfoView(journal: journal, selectedRecordID: nil)
					}
				}
				.toolbar {
					Spacer()
					Button("journal.inspector.toggle", systemImage: "sidebar.trailing") {
						inspectorVisible.toggle()
					}
				}
				.inspectorColumnWidth(min: 400, ideal: 400, max: 500)
			}
	}

	private var _recordList : some View
	{
		Table(cache.displayedItems, children: \JournalRecord.subRecords, selection: $selectedRecordIDs, columnCustomization: nil) {
			TableColumn(txt("journal.table.column.records")) { record in
				_recordRow(for: record)
					.frame(height: 20) // hints the total height, thus fixes issue with initial scrollbar size
			}
		}
		.tableStyle(.automatic)
		.tableColumnHeaders(.hidden)
		.scrollContentBackground(.hidden)
		.scrollBounceBehavior(.basedOnSize, axes: [.horizontal])
		.alternatingRowBackgrounds(.disabled)
		.safeAreaInset(edge: .top, spacing: 0) {
			_controlBar
		}
		.focused($focus, equals: .recordTable)
		.contextMenu(forSelectionType: JournalRecord.ID.self) { items in
			_recordRowContextMenuItems(for: items)
		}
		.confirmationDialog(txt("journal.record.remove.confirmation.title"), isPresented: $showingRemoveConfirmationPrompt) {
			Button(role: .destructive) {
				selectedRecordIDs.subtract(toBeRemovedRecordIDs)
				let itemsToRemove = toBeRemovedRecordIDs
				toBeRemovedRecordIDs.removeAll()
				showingRemoveConfirmationPrompt = false
				withAnimation {
					cache.removeItems(withIDs: itemsToRemove)
				}
			} label: {
				txt("journal.record.remove.confirmation.confirm")
			}
			Button(role: .cancel) {
				showingRemoveConfirmationPrompt = false
			} label: {
				txt("journal.record.remove.confirmation.cancel")
			}
		} message: {
			if toBeRemovedRecordIDs.count == 1,
				let toBeRemovedRecordID = toBeRemovedRecordIDs.first,
				let toBeRemovedRecord = cache.displayedItems.first(where: { $0.id == toBeRemovedRecordID }) {
					if toBeRemovedRecord.subRecords != nil {
						txt("journal.record.remove.confirmation.message.group \(toBeRemovedRecord.title) date \(toBeRemovedRecord.date.bh)")
					} else {
						txt("journal.record.remove.confirmation.message.single \(toBeRemovedRecord.title) date \(toBeRemovedRecord.date.bh)")
					}
				} else {
				txt("journal.record.remove.confirmation.message.multi")
			}
		}
		.dialogSeverity(.critical)
#if os(macOS)
		.onDeleteCommand {
			if !selectedRecordIDs.isEmpty {
				_removeRecords(selectedRecordIDs)
			}
		}
		.onExitCommand { // Escape key on macOS
			selectedRecordIDs.removeAll()
		}
#endif
	}

	@ViewBuilder
	private func _recordRow(for record : JournalRecord) -> some View
	{
		let deemphasize = 0.7
		HStack(alignment: .top, spacing: 14) {
			Text(record.date.bh)
				.monospaced()
				.fixedSize()
				.opacity(deemphasize)
			Text(record.title)
				.frame(alignment: .leading)
			_recordRowContentIndicators(for: record, opacity: deemphasize)
			Spacer()
			if record.title.isEmpty || !record.isBalanced {
				Image(systemName: "exclamationmark.triangle.fill")
					.foregroundColor(.red)
			} else {
				if record.subRecords == nil || record.value > 0 {
					MoneyText(record.value)
						.fixedSize()
						.opacity(deemphasize)
				}
			}
		}
	}

	@ViewBuilder
	private func _recordRowContentIndicators(for record : JournalRecord, opacity : Double) -> some View
	{
		let isTagged = !record.tags
			.filter{ !$0.trimmingCharacters(in: .whitespaces).isEmpty }
			.isEmpty
		HStack(spacing: 4) {
			if isTagged {
				Image(systemName: "tag")
					.opacity(opacity)
					.help(record.tags.joined(separator: ", "))
			}
			if !record.note.isEmpty {
				Image(systemName: "text.bubble")
					.opacity(opacity)
					.help(record.note)
			}
			if !record.links.isEmpty {
				Image(systemName: "link")
					.opacity(opacity)
					.help(record.links.map(\.url).joined(separator: "\n"))
			}
		}
	}

	@ViewBuilder
	private func _recordRowContextMenuItems(for recordIDs : Set<JournalRecord.ID>) -> some View
	{
		if recordIDs.isEmpty { // context menu opened on empty table area
			Button {
				_addRecord()
			} label: {
				txt("journal.action.add_record")
			}
		} else {
			if recordIDs.count >= 1 {
				Button {
					_duplicateRecords(recordIDs)
				} label: {
					txt("journal.action.duplicate_records")
				}

				Button {
					_removeRecords(recordIDs)
				} label: {
					Label(/TR("journal.action.remove_records") + "…", systemImage: "trash")
				}
			}
			if recordIDs.count > 1 {
				Button {
					_groupRecords(recordIDs)
				} label: {
					txt("journal.action.group_records")
				}
			}
			if recordIDs.count >= 1 && _canUngroupAllDisplayedItems(in: recordIDs) {
				Button {
					_ungroupRecords(recordIDs)
				} label: {
					txt("journal.action.ungroup_records")
				}
			}
		}
	}

	@ViewBuilder
	private func _recordEditor(_ selectedRecordBinding : Binding<JournalRecord>) -> some View
	{
		RecordView(record: selectedRecordBinding, allowedPeriod: businessPeriod)
			.padding(EdgeInsets(top: 2, leading: 10, bottom: 20, trailing: 10))
			.environment(\.degaapMapping, chartMapping)
			.focused($focus, equals: .recordEditor)
	}

	private var _controlBar : some View
	{
		HStack
		{
			BHItemAdderButton(bordered: false) {
				_addRecord()
			}
			.help(txt("journal.add_record.help"))

			Spacer()

			if !selectedRecordIDs.isEmpty {
				BHItemRemoverButton {
					_removeRecords(selectedRecordIDs)
				}
			}
		}
		.overlay {
			if showingMappingWaitIndicator
			{
				_waitForMapping
			}
		}
		.padding([.leading, .trailing], 20)
		.frame(height: 40)
		.background(.thinMaterial)
	}

	private var _emptyJournalView : some View
	{
		GeometryReader { geo in
			VStack {
				Spacer()
				if showingCenterWaitIndicator {
					ProgressView()
				} else {
					txt(searchKey.isEmpty ? "journal.empty" : "journal.no_search_match")
						.fixedSize()
				}
				Spacer()
					.frame(height: round(geo.size.height * 0.6))
			}
			.frame(maxWidth: .infinity)
		}
	}

	private func _loadChartMapping(from chart : DEGAAPSourceChart, to taxonomy : DEGAAPTaxonomyVersion) async -> DEGAAPMappingDescriptor?
	{
		showingMappingWaitIndicator = true
		defer{ showingMappingWaitIndicator = false }
		logger.info("Loading DEGAAP mapping from chart \(chart) to taxonomy \(taxonomy.humanReadable)...")
		let mapping = DEGAAPMappingDescriptor(source: chart, target: DEGAAPChartDescriptor(taxonomy: taxonomy))
		guard await degaap.degaap.contains(mapping: mapping) else {
			return nil
		}
		logger.info("Loaded DEGAAP mapping from chart \(chart) to taxonomy \(taxonomy.humanReadable).")
		return mapping
	}

	/// Adds a new record with the given title and a date assigned depending on context.
	///
	///	If the an item is currently selected in the list, the date of that item
	///	will be used for the new ``Record``. If no item is selected and
	///	current system date is within the business period, the current system
	///	date will be used. If none of the above apply, the date with the highest
	///	value.
	///
	/// - parameter title: If not specified, the value of `searchKey` will be used
	///  in order to prevent the new record from being hidden.
	@discardableResult
	private func _addRecord(title : String? = nil) -> JournalRecord.ID
	{
		let date : Day = {
			if let _selectedRecord {
				return _selectedRecord.date
			}
			let today = Day()
			if businessPeriod.contains(today) {
				return today
			}
			return cache.displayedItems.max{ $0.date < $1.date }?.date ?? businessPeriod.start
		}()
		var record = JournalRecord()
		record.date = date
		record.title = title ?? searchKey
		record.debit.append(JournalRecord.Entry(account: .none))
		record.credit.append(JournalRecord.Entry(account: .none))
		let recordID = record.id
		withAnimation {
			cache.displayedItems.append(record)
			Task { @MainActor in
				selectedRecordIDs = Set([recordID])
			}
		}
		return recordID
	}

	private func _removeRecords(_ recordIDs : Set<JournalRecord.ID>)
	{
		toBeRemovedRecordIDs = recordIDs
		withAnimation(.spring()) {
			showingRemoveConfirmationPrompt = true
		}
	}

	private func _duplicateRecords(_ recordIDs : Set<JournalRecord.ID>)
	{
		guard !recordIDs.isEmpty else { return }
		var displayedRecords = [JournalRecord]()
		for record in cache.displayedItems {
			let (copy, duplicate, _) = record.copy(duplicatingRecordsMatching: recordIDs)
			displayedRecords.append(copy)
			if let duplicate {
				displayedRecords.append(duplicate)
			}
		}
		withAnimation {
			cache.displayedItems = displayedRecords
		}
	}

	private func _groupRecords(_ recordIDs : Set<JournalRecord.ID>)
	{
		withAnimation {
			cache.groupRecords(recordIDs)
		}
	}

	private func _ungroupRecords(_ recordIDs : Set<JournalRecord.ID>)
	{
		if _canUngroupAllDisplayedItems(in: recordIDs) {
			selectedRecordIDs.subtract(recordIDs)
			withAnimation {
				cache.ungroupRecords(recordIDs)
			}
		}
	}

	/// - Returns: Whether all items contained in `displayedItems` and whose IDs are in the given set can be ungrouped.
	private func _canUngroupAllDisplayedItems(in recordIDs : Set<JournalRecord.ID>) -> Bool
	{
		guard !recordIDs.isEmpty else { return false }
		var foundMatchingItem = false
		var foundMatchingNonGroupItem = false
		for item in cache.displayedItems {
			if recordIDs.contains(item.id) {
				foundMatchingItem = true
				if (item.subRecords == nil) {
					foundMatchingNonGroupItem = true
					break
				}
			}
		}
		return foundMatchingItem && !foundMatchingNonGroupItem
	}


	private var _waitForMapping : some View
	{
		ViewThatFits(in: .horizontal)
		{
			HStack(spacing: 10)
			{
				ProgressView()
					.controlSize(.small)
				txt("journal.wait.mapping")
			}
			ProgressView()
				.controlSize(.small)
		}
	}

	private var _selectedRecord : JournalRecord?
	{
		guard selectedRecordIDs.count == 1,
			let selectedRecordID = selectedRecordIDs.first
			else { return nil }
		return cache.item(withID: selectedRecordID)
	}
}
