// Copyright 2025 Kai Oezer

import SwiftUI
import Buchhaltung
import DEGAAP

struct JournalPropertiesView: View
{
	@Binding var journal : Journal

	var body: some View
	{
		VStack(alignment: .leading) {
			JournalChartPicker(chartMapping: $journal.chart)
		}
		.padding()
	}
}

#Preview
{
	@Previewable @State var journal : Journal = .init()

	JournalPropertiesView(journal: $journal)
}

// MARK: -

struct JournalChartPicker : View
{
	@Binding var chartMapping : DEGAAPSourceChart

	var body: some View
	{
		Picker(selection: $chartMapping) {
			txt("journal.properties.chart.skr03")
				.tag(DEGAAPSourceChart.skr03)
			txt("journal.properties.chart.skr04")
				.tag(DEGAAPSourceChart.skr04)
			txt("journal.properties.chart.ikr")
				.tag(DEGAAPSourceChart.ikr)
		} label: {
			txt("journal.properties.chart.label")
		}
		.font(.system(.body))
		.fixedSize()
	}
}
