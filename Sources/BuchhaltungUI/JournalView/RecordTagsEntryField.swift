// Copyright 2023-2025 Kai Oezer

import SwiftUI
import Buchhaltung
#if canImport(AppKit)
import AppKit
#endif

struct RecordTagsEntryField : View
{
	@Binding var tags : Set<JournalRecord.Tag>

	var body : some View
	{
#if canImport(AppKit)
		TokenizedTagsField(text: _sortedTags)
#else
		TextField(text: _sortedTags, label: {})
			.multilineTextAlignment(.trailing)
			.textFieldStyle(.plain)
			.font(.system(.footnote))
#endif
	}

	var _sortedTags : Binding<String>
	{
		Binding<String>(
			get: {
				tags.sorted().joined(separator: ", ")
			},
			set: {
				tags = Set($0.components(separatedBy: ","))
			}
		)
	}
}

#if canImport(AppKit)

/// Tokenization is based on using NSTokenField that does not supporting
/// customizing the token background color.
/// In order to display a custom token background color, we would use an
/// `NSTextField` whose field editor (an `NSTextView`) is modified
/// to display attributed strings with the desired background color and
/// with `NSTextAttachment` used for the rounded ends of the tokens.
struct TokenizedTagsField : NSViewRepresentable
{
	let tokenField : NSTokenField
	let delegate : TokenFieldDelegate

	init(text : Binding<String>)
	{
		delegate = TokenFieldDelegate(text: text)
		tokenField = NSTokenField(frame: .zero)
		tokenField.tokenStyle = .squared
		tokenField.drawsBackground = false
		tokenField.isBezeled = false
		tokenField.placeholderString = ""
		tokenField.focusRingType = .none
		tokenField.alignment = .left
		tokenField.stringValue = text.wrappedValue
		tokenField.delegate = delegate
	}

	func makeNSView(context : Context) -> NSTokenField
	{
		tokenField
	}

	func updateNSView(_ tokenField : NSTokenField, context : Context)
	{
		tokenField.stringValue = delegate.text
		tokenField.delegate = delegate
	}

	class TokenFieldDelegate : NSObject, NSTokenFieldDelegate
	{
		@Binding var text : String

		init(text: Binding<String>)
		{
			_text = text
		}

		func controlTextDidEndEditing(_ notification : Notification)
		{
			if let fieldEditorText = (notification.object as? NSTextField)?.stringValue {
				text = fieldEditorText
			}
		}
	}
}

#endif

// MARK: - Preview

#Preview("RecordTagsEditor", traits: .fixedLayout(width: 240, height: 100))
{
	@Previewable @State var tags : Set<JournalRecord.Tag> = ["Project A", "Project B"]

	RecordTagsEntryField(tags: $tags)
		.padding(20)
}
