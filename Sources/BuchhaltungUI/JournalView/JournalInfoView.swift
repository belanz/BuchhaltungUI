// Copyright 2025 Kai Oezer

import SwiftUI
import Charts
import Buchhaltung
import DEGAAP

struct JournalInfoView: View
{
	let journal : Journal
	let selectedRecordID : JournalRecord.ID?
	@State private var _info : JournalInfo = .init()
	@EnvironmentObject var observableDegaap : ObservableDEGAAPEnvironment

	var body : some View
	{
		VStack(alignment: .leading, spacing: 10) {
			JournalBalanceView(info: _info)
			Divider()
			JournalRecordsCountView(count: _info.totalNumberOfRecords)
			JournalPayableTaxView(tax: _info.tax)
		}
		.padding()
		.onChange(of: selectedRecordID, initial: true) {
			Task {
				_info = await JournalInfo.collectInfo(for: journal, upTo: selectedRecordID, degaapEnvironment: observableDegaap.degaap)
			}
		}
	}
}

#Preview("JournalInfoView", traits: .fixedLayout(width: 300, height: 100))
{
	@Previewable var _journal : Journal = .init(records: [
		JournalRecord(date: Day(2025,4,1)!, title: "Some record", debit: [.init(account: .init("1200")!, value: 100)], credit: [.init(account: .init("0800")!, value: 100)])
	])

	JournalInfoView(journal: _journal, selectedRecordID: nil)
		.padding()
		.environmentObject(ObservableDEGAAPEnvironment(degaap: DEGAAPEnvironment()))
}

// MARK: -

struct JournalRecordsCountView : View
{
	let count : Int
	@Environment(\.colorScheme) var colorScheme

	var body : some View
	{
		LabeledContent {
			txt("\(count)")
		} label: {
			txt("journal.info.count.label")
		}
		.foregroundStyle(Color(white: colorScheme == .dark ? 0.4 : 0.7))
	}
}

// MARK: -

struct JournalBalanceView : View
{
	let info : JournalInfo
	@State var copiedToPasteboard : Bool = false
	@Environment(\.colorScheme) var colorScheme : ColorScheme

	var body: some View
	{
		VStack(alignment: .center) {
			Chart(JournalInfo.AccountType.allCases) { accountType in
				BarMark(
					x: .value("journal.info.balance.account", accountType),
					y: .value("journal.info.balance.total", info.balance[accountType])
				)
			}
			.chartXAxis {
				AxisMarks(values: JournalInfo.AccountType.allCases) { value in
					AxisValueLabel(/TR(value.as(JournalInfo.AccountType.self)!.rawValue))
				}
			}
			.foregroundStyle(info.balance.isBalanced ? Color(white: colorScheme == .dark ? 0.3 : 0.85) : Color.red)
		}
		.frame(height: 80)
		.frame(minWidth: 280)
		.help(info.date == .distantFuture ? txt("") : txt("journal.info.balance.help \(info.date.description) \(info.balance.assets.debit.description) \(info.balance.assets.credit.description) \(info.balance.eqLiab.debit.description) \(info.balance.eqLiab.credit.description)"))
		.onTapGesture{ _copyToPasteboard() }
		.overlay {
			if copiedToPasteboard {
				txt("journal.info.balance.pasteboard.notification")
					.padding(8)
					.background(RoundedRectangle(cornerRadius: 8).fill(Color(white: colorScheme == .dark ? 0.5 : 0.9)))
			}
		}
	}

	private func _copyToPasteboard()
	{
		guard info.date != .distantFuture else { return }
		let pasteboardString = /TR("journal.info.balance.pasteboard.string \(info.date.description) \(info.balance.assets.debit.description) \(info.balance.assets.credit.description) \(info.balance.eqLiab.debit.description) \(info.balance.eqLiab.credit.description)")
#if canImport(AppKit)
		NSPasteboard.general.clearContents()
		NSPasteboard.general.setString(pasteboardString, forType: .string)
#elseif canImport(UIKit)
		UIPasteboard.general.string = pasteboardString
#endif
		withAnimation(.easeIn(duration: 0.1)) {
			copiedToPasteboard = true
		} completion: {
			withAnimation(.easeOut(duration: 0.8)) { copiedToPasteboard = false }
		}
	}
}

#Preview("JournalBalanceVisualization", traits: .fixedLayout(width: 300, height: 500))
{
	VStack {
		GroupBox("balanced") {
			JournalBalanceView(info: .init(
				date: Day(2025,3,1)!,
				balance: .init(
					assets: .init(debit: 120, credit: 20),
					eqLiab: .init(debit: 50, credit: 150)
				)
			))
			.padding()
		}
		.padding()
		GroupBox("unbalanced") {
			JournalBalanceView(info: .init(
				date: Day(2025,3,1)!,
				balance: .init(
					assets: .init(debit: 80, credit: 20),
					eqLiab: .init(debit: 50, credit: 150)
				)
			))
			.padding()
		}
		.padding()
	}
}

// MARK: -

struct JournalPayableTaxView : View
{
	let tax : JournalInfo.Tax
	@Environment(\.colorScheme) var colorScheme

	var body: some View
	{
		HStack {
			LabeledContent {
				Text(_payableTax.description + " €")
					.textSelection(.enabled)
			} label: {
				txt("journal.info.vat.label")
			}
			Spacer()
			Image(systemName: "info.circle")
				.help(txt("journal.info.vat.info"))
		}
		.foregroundStyle(Color(white: colorScheme == .dark ? 0.4 : 0.7))
	}

	private var _payableTax : Money
	{
		(tax.payable.credit - tax.payable.debit) - (tax.receivable.debit - tax.receivable.credit)
	}
}

#Preview("JournalPayableVATView")
{
	JournalPayableTaxView(tax: .init(receivable: .init(debit: 100, credit: 3000)))
		.padding()
}

// MARK: -

struct JournalInfo
{
	struct Balance : CustomStringConvertible
	{
		var assets = DebCred()
		var eqLiab = DebCred()

		var isBalanced : Bool { assets.balance + eqLiab.balance == 0 }
		var description : String { "Assets \(assets)\nLiabilities \(eqLiab)" }
		static func += (lhs : inout Self, rhs : Self)
		{
			lhs.assets += rhs.assets
			lhs.eqLiab += rhs.eqLiab
		}

		subscript(_ accountType : AccountType) -> PlottableMoney
		{
			switch accountType
			{
				case .assetsDebit:  return PlottableMoney(assets.debit)
				case .assetsCredit: return PlottableMoney(assets.credit)
				case .eqliabDebit:  return PlottableMoney(eqLiab.debit)
				case .eqliabCredit: return PlottableMoney(eqLiab.credit)
			}
		}
	}

	struct Tax
	{
		var receivable = DebCred()
		var payable = DebCred()
		static func += (lhs : inout Self, rhs : Self)
		{
			lhs.receivable += rhs.receivable
			lhs.payable += rhs.payable
		}
	}

	struct DebCred : CustomStringConvertible
	{
		var debit : Money = .zero
		var credit : Money = .zero
		var balance : Money { debit - credit }
		var description: String { "D:\(debit) - C:\(credit)" }
		static func += (lhs : inout Self, rhs : Self) { lhs.debit += rhs.debit; lhs.credit += rhs.credit }
	}

	enum AccountType : String, Identifiable, Plottable, CaseIterable
	{
		case assetsDebit = "journal.info.balance.assets.debit"
		case assetsCredit = "journal.info.balance.assets.credit"
		case eqliabDebit = "journal.info.balance.eqliab.debit"
		case eqliabCredit = "journal.info.balance.eqliab.credit"

		var id : String { self.rawValue }
	}

	struct PlottableMoney : Plottable, MoneyEquivalent
	{
		var monetaryValue : Money

		init?(primitivePlottable value : UInt32)
		{
			monetaryValue = Money(Int(value), 0)
		}

		init(_ value: Money)
		{
			monetaryValue = value
		}

		var primitivePlottable: UInt32
		{
			assert(monetaryValue >= .zero)
			return UInt32(monetaryValue.absolute.components.0)
		}
	}

	var date : Day = .distantFuture
	var balance : Balance = .init()
	var tax : Tax = .init()
	var totalNumberOfRecords : Int = 0

	static func += (lhs : inout Self, rhs : Self)
	{
		lhs.balance += rhs.balance
		lhs.tax += rhs.tax
	}
}

extension JournalInfo
{
	static func collectInfo(
		for journal : Journal,
		upTo targetRecordID : JournalRecord.ID? = nil,
		degaapEnvironment degaap : DEGAAPEnvironment
	) async -> JournalInfo
	{
		let signpostID = signposter.makeSignpostID()
		let intervalState = signposter.beginInterval("collecting journal info", id: signpostID)

		var info = JournalInfo()
		let chartMapping = DEGAAPMappingDescriptor(source: journal.chart, target: .init(taxonomy: .allCases.last!, fiscal: true))
		let sortedRecords = journal.records.sorted()
		if let lastRecordDate = sortedRecords.last?.date {
			info.date = lastRecordDate
		}
		for record in sortedRecords {
			if await _recursivelyeCollectInfo(for: record, into: &info, upTo: targetRecordID, mapping: chartMapping, degaapEnvironment: degaap) {
				info.date = record.date
				break
			}
		}

		signposter.endInterval("collecting journal info", intervalState)

		return info
	}

	/// Recursively collects the balance sheet contribution of the given record.
	/// - Returns: Whether the given record matches the current selected record.
	/// - Parameter data: The cumulative totals for each balance sheet part
	private static func _recursivelyeCollectInfo(
		for record : JournalRecord,
		into info : inout JournalInfo,
		upTo targetRecordID : JournalRecord.ID?,
		mapping : DEGAAPMappingDescriptor,
		degaapEnvironment degaap : DEGAAPEnvironment
	) async -> Bool
	{
		if let subRecords = record.subRecords {
			for subRecord in subRecords.sorted(by: >) { // note that the sorting needs to be the inverse of the displayed sorting
				if await _recursivelyeCollectInfo(for: subRecord, into: &info, upTo: targetRecordID, mapping: mapping, degaapEnvironment: degaap) {
					return true
				}
			}
		} else {
			info.totalNumberOfRecords += 1
#if true // "do not repeat yourself" versus...
			let processEntries : (JournalRecord,
				KeyPath<JournalRecord, Array<JournalRecord.Entry>>,
				WritableKeyPath<DebCred, Money>
			) async -> JournalInfo = { record, entriesKeyPath, accountKeyPath in
				var info = JournalInfo()
				for entry in record[keyPath: entriesKeyPath] {
					guard let chartItem = await degaap.chartItem(for: entry.account, mapping: mapping) else { continue }
					if chartItem.isAssetAccountID {
						info.balance.assets[keyPath: accountKeyPath] += entry.value
						if chartItem.isReceivableTaxAccountID {
							info.tax.receivable[keyPath: accountKeyPath] += entry.value
						}
					} else if chartItem.isEqLiabAccountID || chartItem.isIncomeOrCostAccountID {
						info.balance.eqLiab[keyPath: accountKeyPath] += entry.value
						if chartItem.isPayableTaxAccountID {
							info.tax.payable[keyPath: accountKeyPath] += entry.value
						}
					}
				}
				return info
			}
			info += await processEntries(record, \.debit.elements, \.debit)
			info += await processEntries(record, \.credit.elements, \.credit)
#else // runtime performance
			for entry in record.debit {
				guard let chartItem = await degaap.chartItem(for: entry.account, mapping: mapping) else { continue }
				if chartItem.isAssetAccountID {
					info.balance.assets.debit += entry.value
					if chartItem.isReceivableTaxAccountID {
						info.tax.receivable.debit += entry.value
					}
				} else if chartItem.isEqLiabAccountID || chartItem.isIncomeOrCostAccountID {
					info.balance.eqLiab.debit += entry.value
					if chartItem.isPayableTaxAccountID {
						info.tax.payable.debit += entry.value
					}
				}
			}
			for entry in record.credit {
				guard let chartItem = await degaap.chartItem(for: entry.account, mapping: mapping) else { continue }
				if chartItem.isAssetAccountID {
					info.balance.assets.credit += entry.value
					if chartItem.isReceivableTaxAccountID {
						info.tax.receivable.credit += entry.value
					}
				} else if chartItem.isEqLiabAccountID || chartItem.isIncomeOrCostAccountID {
					info.balance.eqLiab.credit += entry.value
					if chartItem.isPayableTaxAccountID {
						info.tax.payable.credit += entry.value
					}
				}
			}
#endif
		}
		return record.id == targetRecordID
	}

}
