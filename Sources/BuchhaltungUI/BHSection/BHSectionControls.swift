// Copyright 2023-2025 Kai Oezer

import SwiftUI
import Buchhaltung

struct BHSectionTextField : View
{
	@Binding var text : String
	let id : String
	var instanceID : String = "0"
	var focus : FocusState<String?>.Binding
	var isValid : Bool = true

	@Environment(\.bhErrorColor) var errorColor : Color

	var body : some View
	{
		LabeledContent(/TR(id)) {
			TextField(text: $text, label: {})
				.focused(focus, equals: "\(id).\(instanceID)")
				.multilineTextAlignment(.trailing)
				.foregroundColor(isValid ? .none : errorColor)
				.help(/TR(id + ".help"))
				.labelsHidden()
		}
	}
}

// MARK: -

@MainActor
func bhSectionMoneyField(
	_ binding : Binding<Money>,
	id : String,
	instanceID : String = "0",
	focus: FocusState<String?>.Binding,
	validator : MoneyAmountValidator? = nil
) -> some View
{
	LabeledContent(/TR(id))
	{
		MoneyEntryField(money: binding, amountValidator: validator ?? .nonnegative)
			.focused(focus, equals: "\(id).\(instanceID)")
			.labelsHidden()
	}
}

@MainActor
func bhSectionMoneyText(_ value : Money, id: String) -> some View
{
	LabeledContent(/TR(id))
	{
		HStack
		{
			Spacer()
			MoneyText(value)
		}
	}
}

// MARK: -

@MainActor
func bhSectionDateField(
	_ binding : Binding<Day>,
	id: String,
	instanceID : String = "0",
	focus: FocusState<String?>.Binding
) -> some View
{
	LabeledContent(/TR(id))
	{
		HStack {
			Spacer()
			DayEntryField(day: binding)
				.focused(focus, equals: "\(id).\(instanceID)")
				.labelsHidden()
				.fixedSize()
		}
	}
}

/// Displays a switch and a date field, where the switch is bound to the date field containing a valid date.
@MainActor
func bhSectionOptionalDateField(
	_ day : Binding<Day>,
	id: String,
	instanceID : String = "0",
	focus: FocusState<String?>.Binding,
	defaultDay: Day? = nil
) -> some View
{
	LabeledContent(/TR(id))
	{
		HStack {
			Spacer()
			Toggle(isOn: Binding<Bool>(
				get: { day.wrappedValue.isValid },
				set: { day.wrappedValue = $0 ? (defaultDay ?? Day()) : Day.distantFuture }
			), label: {})
			.labelsHidden()
			DayEntryField(day: day)
				.disabled(!day.wrappedValue.isValid)
				.focused(focus, equals: "\(id).\(instanceID)")
				.labelsHidden()
				.fixedSize()
		}
	}
}

// MARK: -

@MainActor
func bhSectionAdderButton(_ action : @escaping () -> Void) -> BHItemAdderButton
{
	BHItemAdderButton(bordered: false, action: action)
}

struct BHItemAdderButton : View
{
	var bordered = true
	var color = Color.accentColor
	var action : () -> Void

	var body : some View
	{
		Button {
			withAnimation(.spring()) {
				action()
			}
		} label: {
			Image(systemName: "plus")
#if os(macOS)
				.symbolVariant(.circle.fill)
#else
				.symbolVariant(.circle)
#endif
				.foregroundColor(color)
		}
		.modifier(ItemButtonModifier(bordered: bordered))
	}
}

struct BHItemRemoverButton : View
{
	var bordered = false
	var color : Color?
	var action : () -> Void

	var body : some View
	{
		Button {
			withAnimation(.spring()) {
				action()
			}
		} label: {
			Image(systemName: "trash")
#if os(macOS)
				.symbolVariant(.circle.fill)
#else
				.symbolVariant(.circle)
#endif
				.foregroundColor(color ?? .red)
				.opacity(color == nil ? 0.7 : 1.0)
		}
		.modifier(ItemButtonModifier(bordered: bordered))
	}
}

struct ItemButtonModifier : ViewModifier
{
	let bordered : Bool

	func body(content: Content) -> some View
	{
		if bordered
		{
			content.buttonStyle(.bordered)
		}
		else
		{
			content.buttonStyle(.borderless)
		}
	}
}

// MARK: -

@MainActor
func bhSectionPicker<T : Hashable>(
	_ binding : Binding<T>,
	id: String,
	textAndTags : [(LocalizedStringKey, T)]
) -> some View
{
	LabeledContent(/TR(id))
	{
		HStack
		{
			Spacer()
			bhUnlabeledSectionPicker(binding, textAndTags: textAndTags)
		}
	}
}

func bhUnlabeledSectionPicker<T : Hashable>(
	_ binding : Binding<T>,
	textAndTags : [(LocalizedStringKey, T)]
) -> some View
{
	Picker(selection: binding) {
		ForEach(textAndTags, id: \.1) { taggedText in
			txt(taggedText.0)
				.tag(taggedText.1)
		}
	} label: {
	}
	.labelsHidden()
	.fixedSize()
}

func bhUnlabeledUnlocalizedSectionPicker<T : Hashable>(
	_ binding : Binding<T>,
	textAndTags : [(String, T)]
) -> some View
{
	Picker(selection: binding) {
		ForEach(textAndTags, id: \.1) { taggedText in
			Text(taggedText.0)
				.tag(taggedText.1)
		}
	} label: {
	}
	.labelsHidden()
	.fixedSize()
}

// MARK: -

typealias TransactionValidator = (Buchhaltung.Transaction) -> Bool

struct BHSectionTransaction : View
{
	@Binding var bhTransaction : Buchhaltung.Transaction
	var id : String
	var instanceID : String = "0"
	var focus : FocusState<String?>.Binding
	var validator : TransactionValidator? = nil

	var body : some View
	{
		LabeledContent(/TR(id))
		{
			HStack(alignment: .lastTextBaseline)
			{
				Spacer()
				MoneyEntryField(money: $bhTransaction.value)
					.focused(focus, equals: "\(id).\(instanceID).money")
					.labelsHidden()
					.fixedSize()
				DayEntryField(day: $bhTransaction.date)
					.focused(focus, equals: "\(id).\(instanceID).date")
					.labelsHidden()
					.fixedSize()
			}
		}
	}
}

#Preview("BHSectionTransaction")
{
	@Previewable @State var transaction = Buchhaltung.Transaction(date: Day(2023, 6, 3)!, value: 100)
	@Previewable @FocusState var focus : String?

	BHSectionTransaction(bhTransaction: $transaction, id: "test", focus: $focus)
		.padding(20)
}

struct BHSectionTransactionList : View
{
	@Binding var bhTransactions : [Buchhaltung.Transaction]
	var id : String
	var instanceID : String = "0"
	var focus : FocusState<String?>.Binding
	var validator : TransactionValidator? = nil

	var body : some View
	{
		if bhTransactions.isEmpty {
			LabeledContent(/TR(id)) {
				HStack {
					Spacer()
					_transactionsButton("plus") {
						bhTransactions.append(Transaction(date: Day(), value: .zero))
					}
				}
			}
		} else {
			ForEach(_indexedTransactions, id: \.index) { indexedTransaction in
				LabeledContent(/TR(id))
				{
					HStack(alignment: .lastTextBaseline)
					{
						Spacer()

						_transactionsButton("plus") {
							bhTransactions.append(.init(date: indexedTransaction.wrappedValue.item.date.nextYear, value: .zero))
						}

						MoneyEntryField(money: indexedTransaction.item.value)
							.focused(focus, equals: "\(id).\(instanceID).\(indexedTransaction.index).money")
							.labelsHidden()
							.fixedSize()
						DayEntryField(day: indexedTransaction.item.date)
							.focused(focus, equals: "\(id).\(instanceID).\(indexedTransaction.index).date")
							.labelsHidden()
							.fixedSize()

						_transactionsButton("trash") {
							focus.wrappedValue = nil
							bhTransactions.removeAll{ $0 == indexedTransaction.wrappedValue.item }
						}
					}
					.bhInsertRemoveItemTransition()
				}
			}
		}
	}

	private func _transactionsButton(_ imageName : String, action: @escaping () -> Void) -> some View
	{
		Button {
			withAnimation(.spring()) {
				action()
			}
		} label: {
			Image(systemName: imageName)
			#if os(macOS)
				.symbolVariant(.circle.fill)
			#else
				.symbolVariant(.circle)
			#endif
		}
		.buttonStyle(.borderless)
	}

	private var _indexedTransactions : Binding< [IndexedTransaction] >
	{
		Binding< [IndexedTransaction] >(
			get: { bhTransactions.enumerated().map{ IndexedTransaction(index: $0.offset, item: $0.element) } },
			set: { bhTransactions = $0.map{ $0.item } }
		)
	}

	private struct IndexedTransaction : Identifiable, Comparable
	{
		var index : Int
		var item : Buchhaltung.Transaction

		var id : Int { index }

		public static func < (lhs: IndexedTransaction, rhs: IndexedTransaction) -> Bool
		{
			lhs.index < rhs.index
		}
	}
}
