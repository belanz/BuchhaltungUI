// Copyright 2023 Kai Oezer

import SwiftUI

/// View template that contains useful functions for section views
struct BHSectionContainer<Content : View> : View
{
	let content : Content

	init(@ViewBuilder _ content : () -> Content)
	{
		self.content = content()
	}

	var body : some View
	{
		Form
		{
			content
		}
		.formStyle(.grouped)
		.scrollContentBackground(.hidden)
	}
}

struct BHSubsection<Content : View> : View
{
	let title : LocalizedStringKey
	let content : Content

	init(_ title : LocalizedStringKey, @ViewBuilder _ content : () -> Content)
	{
		self.title = title
		self.content = content()
	}

	var body : some View
	{
		Section {
			content
		} header: {
			txt(title)
				.font(.title)
		}
	}
}

struct BHSubsectionHeader<Accessory : View> : View
{
	let title : LocalizedStringKey
	let accessory : Accessory
	let hasTopSpacer : Bool
	@Environment(\.bhHighlightBackgroundColor) var backgroundColor : Color

	init(_ title : LocalizedStringKey, hasTopSpacer : Bool = false, @ViewBuilder _ accessory : () -> Accessory)
	{
		self.title = title
		self.hasTopSpacer = hasTopSpacer
		self.accessory = accessory()
	}

	var body : some View
	{
		Section {
			HStack {
				txt(title)
					.font(.title)
				Spacer()
				accessory
			}
		} header: {
			if hasTopSpacer {
				Spacer()
					.frame(height: 10)
			} else {
				EmptyView()
			}
		}
		.listRowBackground(backgroundColor) // works on iOS only
	}
}
