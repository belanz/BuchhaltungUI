// Copyright 2022-2025 Kai Oezer

import SwiftUI

/// A component for displaying the vertically stacked item attributes within an expandable disclosure group.
///
/// A remove button is displayed at the end of the item's attributes list.
/// This component is created for use in SwiftUI ``Form``s when listing items that can be
/// edited individually.
struct BHSectionItemView<Content : View> : View
{
	let title : String
	var expandedTitle : String? = nil
	var itemDescription : String? = nil
	let content : () -> Content
	let removeAction : () -> Void
	var requiresAttention : Bool = false
	var focus : FocusState<String?>.Binding

	@State private var _isExpanded : Bool = false
	@State private var _showsConfirmationAlert : Bool = false
	@Environment(\.bhErrorColor) var errorColor : Color

	var body: some View
	{
		DisclosureGroup(isExpanded: $_isExpanded, content: _removable)
		{
			HStack
			{
				Text(try! AttributedString(markdown: _isExpanded ? (expandedTitle ?? title) : title))
					.foregroundColor(requiresAttention ? errorColor : .none)
				Spacer()
			}
			.onTapGesture(count: 2) { withAnimation{ _isExpanded.toggle() } }
		}
		.confirmationDialog(txt("item.confirm_removal.title"), isPresented: $_showsConfirmationAlert) {
			Button(role: .destructive) {
				withAnimation{ removeAction() }
				_showsConfirmationAlert = false
			} label: {
				txt("item.confirm_removal.confirm")
			}
			Button(role: .cancel) {
				_showsConfirmationAlert = false
			} label: {
				txt("item.confirm_removal.cancel")
			}
		} message: {
			txt("item.confirm_removal.message \(itemDescription ?? title)")
		}
		.dialogSeverity(.critical)
		.onAppear {
			_isExpanded = requiresAttention
		}
	}

	private func _removable() -> some View
	{
		VStack
		{
			Spacer().frame(height: 8)
			content()
		#if os(iOS)
			Spacer().frame(height: 16)
		#endif
			HStack
			{
				Spacer()
				BHItemRemoverButton {
					focus.wrappedValue = nil
					_showsConfirmationAlert = true
				}
			}
		#if os(iOS)
			Spacer().frame(height: 8)
		#endif
		}
	}
}

// MARK: -

#Preview("BHSectionItemView containing two text fields", traits: .fixedLayout(width: 300, height: 360))
{
	@Previewable @State var name : String = ""
	@Previewable @State var address : String = ""
	@Previewable @FocusState var focused : String?
	@Previewable let expanded : Bool = false

	VStack(alignment: .leading)
	{
		sectionItemPreviewWithTextFields(expanded: false, $name, $address, $focused)
		Divider()
		sectionItemPreviewWithTextFields(expanded: true, $name, $address, $focused)
		Spacer()
	}
	.navigationTitle("BHSectionView preview")
}

@MainActor
@ViewBuilder
func sectionItemPreviewWithTextFields(
	expanded : Bool,
	_ name : Binding<String>,
	_ address : Binding<String>,
	_ focused : FocusState<String?>.Binding
) -> some View
{
	BHSectionItemView(
		title: "Test",
		content: {
			VStack {
				TextField(text: name) {
					Text(verbatim: "Name")
				}
				TextField(text: address) {
					Text(verbatim: "Address")
				}
			}
		},
		removeAction: { /* do nothing */ },
		requiresAttention: expanded,
		focus: focused
	)
	.padding()
}
