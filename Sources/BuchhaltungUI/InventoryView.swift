//  Copyright 2020-2025 Kai Oezer

import SwiftUI
import Buchhaltung

struct InventoryView : View
{
	let businessPeriod : Period
	@Binding var assets : Assets
	@Binding var debts : Debts
	let baseAssets : Assets?
	let baseDebts : Debts?
	@State var showsBaseAssetsPrompt : Bool = false
	@State var showsBaseDebtsPrompt : Bool = false
	@FocusState var focusedControl : String?
	@State var searchKey : String = ""
	@Environment(\.bhErrorColor) var errorColor : Color

	var body : some View
	{
		BHSectionContainer
		{
			_fixedAssets
			_currentAssets
			_longAndShortTermDebts
			_total
		}
		.searchable(text: $searchKey)
	}

	@ViewBuilder
	private var _fixedAssets : some View
	{
		BHSubsectionHeader("assets.fixed") {
			MoneyText(_fixedAssetsTotalValue)
		}
		.valueTrackingOverlayButton(
			value: $assets,
			observer: { updatedAssets in
				showsBaseAssetsPrompt = baseAssets != nil
					&& (!(baseAssets!.fixedAssets).isEmpty || !(baseAssets!.currentAssets).isEmpty)
					&& updatedAssets.fixedAssets.isEmpty
					&& updatedAssets.currentAssets.isEmpty
			},
			isVisible: showsBaseAssetsPrompt && baseAssets != nil,
			text: txt("base_values.apply"),
			topMargin: 8,
			trailingMargin: 80
		) {
			if let baseAssets {
				assets = baseAssets
			}
		}

		_fixedAssetListing(
			defaultAssetType: .intangible,
			assetTypeFilter: { $0.isIntangible },
			title: "assets.fixed.intangibles",
			content: { asset in { _intangibleFixedAssetView(asset) } },
			validation: { $0.name.isEmpty || !$0.acquisitionDate.isValid }
		)

		_fixedAssetListing(
			defaultAssetType: .equipment,
			assetTypeFilter: { $0.isTangible },
			title: "assets.fixed.tangibles",
			content: { asset in { _tangibleFixedAssetView(asset) } },
			validation: { $0.name.isEmpty || $0.registrationID.isEmpty || !$0.acquisitionDate.isValid } )

		_fixedAssetListing(
			defaultAssetType: .financial,
			assetTypeFilter: { $0.isFinancial },
			title: "assets.fixed.financial",
			content: { asset in { _financialFixedAssetView(asset) } },
			validation: { $0.name.isEmpty || !$0.acquisitionDate.isValid }
		)
	}

	typealias AssetTypeFilter = (Assets.FixedAssetType) -> Bool

	private func _fixedAssetListing(
		defaultAssetType : Assets.FixedAssetType,
		assetTypeFilter : @escaping AssetTypeFilter,
		title : LocalizedStringKey,
		content: @escaping (Binding<Assets.FixedAsset>) -> (() -> some View),
		validation : @escaping (Assets.FixedAsset) -> Bool
	) -> some View
	{
		Section
		{
			ForEach(_sortedFilteredFixedAssets(assetTypeFilter)) { asset in
				let asset_ = asset.wrappedValue
				let hasUniqueRegistrationID = asset_.registrationID.isEmpty || assets.fixedAssets.first{ ($0.registrationID == asset_.registrationID) && ($0.id != asset_.id) } == nil
				let titleRegID = asset_.registrationID.isEmpty ? "#???" :  "#**\(asset_.registrationID)**\(hasUniqueRegistrationID ? "" : "!!!")"
				let itemName = asset_.name.isEmpty ? /TR("assets.fixed.name.placeholder") : asset_.name
				let title = "\(titleRegID) **\(itemName)**"
				let titleWithBookValue = title + "   €\(asset_.bookValue(for: businessPeriod.end).bh)"
				BHSectionItemView(
					title: titleWithBookValue,
					expandedTitle: title,
					itemDescription: itemName,
					content: content(asset),
					removeAction: { assets.remove(fixedAsset: asset_) },
					requiresAttention: validation(asset_) || !hasUniqueRegistrationID,
					focus: $focusedControl
				)
			}
			bhSectionAdderButton{ _addFixedAsset(defaultAssetType) }
		} header: {
			HStack {
				txt(title)
					.font(.title2)
				Spacer()
				MoneyText(assets.fixedAssets.filter{ assetTypeFilter($0.type) }.reduce(0){$0 + $1.bookValue(for: businessPeriod.end)})
					.opacity(0.7)
			}
		}
	}

	@ViewBuilder
	private var _currentAssets : some View
	{
		BHSubsectionHeader("assets.current", hasTopSpacer: true) {
			MoneyText(_currentAssetsTotalValue)
		}

		Section
		{
			ForEach(_sortedFilteredCurrentAssets) { asset in
				let asset_ = asset.wrappedValue
				let itemName = asset_.name.isEmpty ? /TR("assets.fixed.name.placeholder") : asset_.name
				let title = "**\(itemName)**"
				let titleWithValue = title + "   €\(asset_.value.bh)"
				BHSectionItemView(
					title: titleWithValue,
					expandedTitle: title,
					itemDescription: itemName,
					content: { _currentAssetsView(asset) },
					removeAction: { assets.remove(currentAsset: asset_) },
					requiresAttention: asset_.name.isEmpty,
					focus: $focusedControl
				)
				.id(asset_.id)
			}

			bhSectionAdderButton{ _addCurrentAsset() }
		}
	}

	typealias DebtFilter = (Debt) -> Bool

	@ViewBuilder
	private var _longAndShortTermDebts : some View // do not rename to `_debts` due to existing variable `debts`.
	{
		BHSubsectionHeader("debts", hasTopSpacer: true) {
			MoneyText(_debtsTotalValue)
		}
		.valueTrackingOverlayButton(
			value: $debts,
			observer: { updatedDebts in
				showsBaseDebtsPrompt = (baseDebts != nil && !(baseDebts!.debts).isEmpty)
					&& updatedDebts.debts.isEmpty
			},
			isVisible: showsBaseDebtsPrompt && baseDebts != nil,
			text: txt("base_values.apply"),
			topMargin: 8,
			trailingMargin: 80
		) {
			if let baseDebts {
				debts = baseDebts
			}
		}

		ForEach(_sortedDebts) { debt in
			let debt_ = debt.wrappedValue
			let title = debt_.name
			let titleWithValue = title + "   €\(debt_.value(for: businessPeriod.end).bh)"
			BHSectionItemView(
				title: titleWithValue,
				expandedTitle: title,
				content: { _debtView(debt) },
				removeAction: { debts.debts.remove(debt_) },
				requiresAttention: debt_.name.isEmpty || !debt_.isValid,
				focus: $focusedControl)
		}

		bhSectionAdderButton{ _addDebt() }
	}

	private var _total : some View
	{
		BHSubsectionHeader("inventory.total", hasTopSpacer: true) {
			MoneyText(_fixedAssetsTotalValue + _currentAssetsTotalValue - _debtsTotalValue)
		}
	}

	@ViewBuilder
	private func _intangibleFixedAssetView(_ asset : Binding<Assets.FixedAsset>) -> some View
	{
		let id = asset.wrappedValue.id.uuidString
		Form
		{
			BHSectionTextField(text: asset.registrationID, id: "assets.fixed.id", instanceID: id, focus: $focusedControl)
			BHSectionTextField(text: asset.name, id: "assets.fixed.name", instanceID: id, focus: $focusedControl)
			bhSectionDateField(asset.acquisitionDate, id: "assets.fixed.acquisition_date", instanceID: id, focus: $focusedControl)
			bhSectionMoneyField(asset.acquisitionCost, id: "assets.fixed.acquisition_cost", instanceID: id, focus: $focusedControl)
			bhSectionOptionalDateField(asset.retirementDate, id: "assets.fixed.retirement_date", instanceID: id, focus: $focusedControl, defaultDay: businessPeriod.end)
		}
		.formStyle(.columns)
	}

	@ViewBuilder
	private func _tangibleFixedAssetView(_ asset : Binding<Assets.FixedAsset>) -> some View
	{
		let id = asset.wrappedValue.id.uuidString
		Form
		{
			BHSectionTextField(text: asset.registrationID, id: "assets.fixed.id", instanceID: id, focus: $focusedControl)
			bhSectionPicker(asset.type, id: "assets.fixed.tangible.type", textAndTags: [
				("assets.fixed.tangible.type.land", .land),
				("assets.fixed.tangible.type.buildings", .building),
				("assets.fixed.tangible.type.machinery", .machinery),
				("assets.fixed.tangible.type.equipment.office", .officeEquipment),
				("assets.fixed.tangible.type.equipment.factory", .factoryEquipment),
				("assets.fixed.tangible.type.equipment.other", .equipment),
				("assets.fixed.tangible.type.vehicle.passenger", .passengerVehicle),
				("assets.fixed.tangible.type.vehicle.commercial", .commercialVehicle),
				("assets.fixed.tangible.type.vehicle.other", .vehicle),
				("assets.fixed.tangible.type.other", .other)
			])
			BHSectionTextField(text: asset.name, id: "assets.fixed.name", instanceID: id, focus: $focusedControl)
			bhSectionDateField(asset.acquisitionDate, id: "assets.fixed.acquisition_date", instanceID: id, focus: $focusedControl)
			bhSectionMoneyField(asset.acquisitionCost, id: "assets.fixed.acquisition_cost", instanceID: id, focus: $focusedControl)
			bhSectionOptionalDateField(asset.retirementDate, id: "assets.fixed.retirement_date", instanceID: id, focus: $focusedControl, defaultDay: businessPeriod.end)
			_assetDepreciationSelector(asset.depreciationSchedule, id: "assets.fixed.depreciation")
			bhSectionMoneyText(asset.wrappedValue.bookValue(for: businessPeriod.end), id: "assets.fixed.book_value")
		}
		.formStyle(.columns)
	}

	@ViewBuilder
	private func _financialFixedAssetView(_ asset : Binding<Assets.FixedAsset>) -> some View
	{
		let id = asset.wrappedValue.id.uuidString
		Form
		{
			BHSectionTextField(text: asset.name, id: "assets.fixed.name", instanceID: id, focus: $focusedControl)
			bhSectionDateField(asset.acquisitionDate, id: "assets.fixed.acquisition_date", instanceID: id, focus: $focusedControl)
			bhSectionMoneyField(asset.acquisitionCost, id: "assets.fixed.acquisition_cost", instanceID: id, focus: $focusedControl)
			bhSectionOptionalDateField(asset.retirementDate, id: "assets.fixed.retirement_date", instanceID: id, focus: $focusedControl, defaultDay: businessPeriod.end)
		}
		.formStyle(.columns)
	}

	@ViewBuilder
	private func _currentAssetsView(_ asset : Binding<Assets.CurrentAsset>) -> some View
	{
		let instanceID = asset.id.uuidString
		Form
		{
			bhSectionPicker(asset.type, id: "assets.current.type", textAndTags: [
				("assets.current.type.material", .material),
				("assets.current.type.goods.in_progress", .inProgress),
				("assets.current.type.goods.finished", .finishedGoods),
				("assets.current.type.receivable", .receivable),
				("assets.current.type.security", .security),
				("assets.current.type.cash_equivalent", .cashEquivalent),
				("assets.current.type.other", .other)
			])
			BHSectionTextField(text: asset.name, id: "assets.current.name", instanceID: instanceID, focus: $focusedControl, isValid: !asset.wrappedValue.name.isEmpty)
			bhSectionMoneyField(asset.value, id: "assets.current.value", instanceID: instanceID, focus: $focusedControl)
		}
		.formStyle(.columns)
	}

	@ViewBuilder
	private func _debtView(_ debt : Binding<Debt>) -> some View
	{
		let instanceID = debt.wrappedValue.id.uuidString
		Form
		{
			BHSectionTextField(text: debt.name, id: "debt.name", instanceID: instanceID, focus: $focusedControl)
			BHSectionTransaction(bhTransaction: debt.loan, id: "debt.loan", instanceID: instanceID, focus: $focusedControl, validator: { _ in debt.wrappedValue.isValid})
			BHSectionTransactionList(bhTransactions: debt.payments, id: "debt.payment", instanceID: instanceID, focus: $focusedControl, validator: { _ in debt.wrappedValue.isValid })
		}
		.formStyle(.columns)
	}

	private var _debtsTotalValue : Money
	{
		debts.debts.reduce(Money.zero){ $0 + $1.value(for: businessPeriod.end) }
	}

	private var _currentAssetsTotalValue : Money
	{
		assets.currentAssets.reduce(0){$0 + $1.value}
	}

	private var _fixedAssetsTotalValue : Money
	{
		assets.fixedAssets.reduce(0){ $0 + $1.bookValue(for: businessPeriod.end) }
	}

	private func _sortedFilteredFixedAssets(_ ofType : @escaping AssetTypeFilter) -> Binding< [Assets.FixedAsset] >
	{
		let searchFilter : (Assets.FixedAsset) -> Bool = {
			$0.name.lowercased(with: .current).contains(searchKey.lowercased(with: .current))
		}
		return Binding< [Assets.FixedAsset] >(
			get: {
				searchKey.isEmpty
					? assets.fixedAssets.filter{ ofType($0.type) }.sorted()
					: assets.fixedAssets.filter{ ofType($0.type) }.filter(searchFilter).sorted()
			},
			set: {
				assets.fixedAssets = searchKey.isEmpty
					? Set($0 + assets.fixedAssets.filter{ !ofType($0.type) })
					: Set($0 + assets.fixedAssets.filter{ !ofType($0.type) } + assets.fixedAssets.filter{ ofType($0.type) && !searchFilter($0) }
				)
			}
		)
	}

	private func _addFixedAsset(_ type : Assets.FixedAssetType)
	{
		let maxAssignedNumber = assets.fixedAssets.reduce(0) { max($0, Int($1.registrationID) ?? 0) }
		let depreciation = Assets.DepreciationSchedule(progression: type.isTangible ? .linear : .none)
		let newAsset = Assets.FixedAsset(registrationID: "\(maxAssignedNumber + 1)", type: type, acquisitionDate: businessPeriod.start, depreciationSchedule: depreciation)
		assets.add(fixedAsset: newAsset)
	}

	private func _assetDepreciationSelector(_ schedule : Binding<Assets.DepreciationSchedule>, id : String) -> some View
	{
		LabeledContent(/TR(id))
		{
			HStack(alignment: .firstTextBaseline, spacing: 12)
			{
				Spacer()
				bhUnlabeledSectionPicker(schedule.progression, textAndTags: [
					("assets.fixed.depreciation.schedule.none", .none),
					("assets.fixed.depreciation.schedule.linear", .linear),
					("assets.fixed.depreciation.schedule.degressive", .degressive),
					("assets.fixed.depreciation.schedule.gwg", .gwg),
					("assets.fixed.depreciation.schedule.gwg.pool", .pooled)
				])

				if schedule.wrappedValue.progression == .linear
					|| schedule.wrappedValue.progression == .degressive {
					bhUnlabeledUnlocalizedSectionPicker(
						Binding<Int>(
							get: { Int(schedule.wrappedValue.depreciationYears) },
							set: { schedule.wrappedValue.depreciationYears = UInt($0) }
						),
						textAndTags: (1..<21).map{ (String($0), $0) }
					)
					txt("assets.fixed.depreciation.years")
				}
			}
		}
	}

	private var _sortedFilteredCurrentAssets : Binding< [Assets.CurrentAsset] >
	{
		let searchFilter : (Assets.CurrentAsset) -> Bool = {
			$0.name.lowercased(with: .current).contains(searchKey.lowercased(with: .current))
		}
		return Binding< [Assets.CurrentAsset] >(
			get: {
				return searchKey.isEmpty
					? assets.currentAssets.sorted()
					: assets.currentAssets.filter(searchFilter).sorted()
			},
			set: {
				assets.currentAssets = searchKey.isEmpty ? Set($0)
					: Set($0 + assets.currentAssets.filter{ !searchFilter($0) })
			}
		)
	}

	private func _addCurrentAsset()
	{
		let newAsset = Assets.CurrentAsset()
		assets.add(currentAsset: newAsset)
	}

	private var _sortedDebts : Binding<[Debt]>
	{
		Binding<[Debt]>(
			get: {
				debts.debts.sorted()
			},
			set: {
				debts.debts = Set($0)
			}
		)
	}

	private func _addDebt()
	{
		let placeholderBase = /TR("debt.name.placeholder")
		let placeholderPattern = try! Regex("^\(placeholderBase) (\\d*)$")
		let counter : Int = debts.debts.reduce(0, {
			if let match = $1.name.wholeMatch(of: placeholderPattern), match.count == 2 {
				return max($0, Int(match.last?.substring ?? "0") ?? 0)
			}
			return $0
		})
		let newDebt = Debt(name: placeholderBase + " \(counter + 1)",
			loan: .init(date: businessPeriod.start, value: .zero),
			payments: [.init(date: Day().nextYear - 1, value: .zero)]
		)
		assert(!debts.debts.contains(newDebt))
		debts.debts.insert(newDebt)
	}
}

// MARK: - Preview

#Preview("Assets view", traits: .fixedLayout(width: 600, height: 500))
{
	lazy var assets : Assets = {
		var assets = Assets()
		assets.fixedAssets.insert(Assets.FixedAsset(registrationID: "1", name: "Apple Mac Studio", acquisitionCost: 3300))
		assets.currentAssets.insert(Assets.CurrentAsset())
		return assets
	}()

	lazy var debts : Debts = {
		var debts = Debts()
		debts.debts.insert(Debt(name: "Loan 1", loan: .init(date: Day(2023,3,4)!, value: 450), payments: [.init(date: Day(2023,9,3)!, value: 500)]))
		return debts
	}()

	InventoryView(
		businessPeriod: Period(start: Day(2021, 1, 1)!, end: Day(2021, 12, 31)!),
		assets: Binding<Assets>( get: { assets }, set: { assets = $0 }),
		debts: Binding<Debts>( get: { debts}, set: {debts = $0} ),
		baseAssets: nil,
		baseDebts: nil
	)
}
