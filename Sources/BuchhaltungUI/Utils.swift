// Copyright 2023-2025 Kai Oezer

import SwiftUI

// MARK: Localization

public func txt(_ loc : LocalizedStringKey) -> Text
{
	Text(loc, bundle: .module)
}

public typealias TR = Translator
public typealias LTR = LazyTranslator

/// A localization utility type for localizable strings contained
/// in a localization table within the BuchhaltungUI bundle.
///
/// Supports localization keys with interpolations.
/// Looks up the translation table immediately upon initialization.
///
/// Example usage with short syntax:
/// ```
/// print(/TR("my.localization.key"))
/// ```
///
/// - Note: An initializer with an argument of type
/// `LocalizedStringKey` would be useful since the passed string
/// literals would be picked up by Xcode and added to the localization
/// table automatically. However, _LocalizedStringKey_ is not designed
/// to be convertible to _String_. Also, if a non-default localization
/// table is used, Xcode would add the keys to the wrong table.
public struct Translator : ExpressibleByStringInterpolation
{
	private let _localized : String

	public init(_ value : String, table : String = "Localizable")
	{
		let locValue = String.LocalizationValue(value)
		_localized = String(localized: locValue, table: table, bundle: .module, locale: .autoupdatingCurrent)
	}

	public init(stringLiteral value: String.LocalizationValue.StringLiteralType)
	{
		self.init(stringLiteral: value, table: "Localizable")
	}

	public init(stringLiteral value: String.LocalizationValue.StringLiteralType, table : String)
	{
		let locValue = String.LocalizationValue(stringLiteral: value)
		_localized = String(localized: locValue, table: table, bundle: .module, locale: .autoupdatingCurrent)
	}

	public init(_ value : StaticString, table : String = "Localizable")
	{
		_localized = String(localized: value, defaultValue: String.LocalizationValue(stringLiteral: value.description), table: table, bundle: .module, locale: .autoupdatingCurrent)
	}

	public init(stringInterpolation value : String.LocalizationValue.StringInterpolation)
	{
		self.init(stringInterpolation: value, table: "Localizable")
	}

	public init(stringInterpolation value : String.LocalizationValue.StringInterpolation, table : String)
	{
		let interpolatedValue = String.LocalizationValue(stringInterpolation: value)
		_localized = String(localized: interpolatedValue, table: table, bundle: .module, locale: .autoupdatingCurrent)
	}
}

extension Translator : CustomStringConvertible
{
	public var description : String
	{
		_localized
	}
}

/// Looks up the translation table when the `description` property is accessed.
public struct LazyTranslator : ExpressibleByStringInterpolation
{
	private let _locResource : LocalizedStringResource

	public init(_ value : String, table : String = "Localizable")
	{
		let locValue = String.LocalizationValue(value)
		_locResource = LocalizedStringResource(locValue, table: table, locale: .autoupdatingCurrent, bundle: .atURL(Bundle.module.bundleURL))
	}

	public init(_ value : StaticString, table : String = "Localizable")
	{
		_locResource = LocalizedStringResource(value, defaultValue: "!", table: table, locale: .autoupdatingCurrent, bundle: .atURL(Bundle.module.bundleURL))
	}

	public init(stringLiteral value : String.LocalizationValue.StringLiteralType)
	{
		self.init(stringLiteral: value, table: "Localizable")
	}

	public init(stringLiteral value : String.LocalizationValue.StringLiteralType, table : String)
	{
		let locValue = String.LocalizationValue(stringLiteral: value)
		_locResource = LocalizedStringResource(locValue, table: table, locale: .autoupdatingCurrent, bundle: .atURL(Bundle.module.bundleURL))
	}

	public init(stringInterpolation value : String.LocalizationValue.StringInterpolation)
	{
		self.init(stringInterpolation: value, table: "Localizable")
	}

	public init(stringInterpolation value : String.LocalizationValue.StringInterpolation, table : String)
	{
		let interpolatedValue = String.LocalizationValue(stringInterpolation: value)
		_locResource = LocalizedStringResource(interpolatedValue, table: table, locale: .autoupdatingCurrent, bundle: .atURL(Bundle.module.bundleURL))
	}
}

extension LazyTranslator : CustomStringConvertible
{
	public var description : String
	{
		String(localized: _locResource)
	}
}

prefix operator /

extension Translator
{
	/// Provides a compact syntax for localizing text with `Translator`.
	public static prefix func / (tr : Translator) -> String
	{
		tr.description
	}
}

extension LazyTranslator
{
	/// Provides a compact syntax for localizing text with `LazyTranslator`.
	public static prefix func / (tr : LazyTranslator) -> String
	{
		tr.description
	}
}

// MARK: - UI

public var verticalLine : some View
{
	lineColor
		.padding(0)
		.frame(width: 1)
		.frame(maxHeight: .infinity)
}

public var horizontalLine : some View
{
	lineColor
		.padding(0)
		.frame(height: 1)
		.frame(maxWidth: .infinity)
}

fileprivate let lineColor = Color(white: 0.8)

// MARK: -

import OSLog

let logger = os.Logger(subsystem: "belanz.BuchhaltungUI", category: "BuchhaltungUI")


fileprivate let _signposter = OSSignposter(subsystem: "belanz.BuchhaltungUI", category: "BuchhaltungUI")

var signposter : OSSignposter
{
#if !DEBUG
	_signposter.isEnabled = false
#endif
	return _signposter
}
