// Copyright 2023-2025 Kai Oezer

import SwiftUI
import Buchhaltung

public struct BuchhaltungViews
{
	@Binding var ledger : Ledger

	public init(ledger: Binding<Ledger>)
	{
		_ledger = ledger
	}

	@MainActor
	public var optionsBar : some View
	{
		OptionsBar(ledger: $ledger)
	}

	@MainActor
	public var journalView : some View
	{
		JournalView(
			journal: $ledger.journal,
			taxonomy: $ledger.taxonomy,
			businessPeriod: ledger.period,
			showingCenterWaitIndicator: !ledger.journal.items.isEmpty
		)
	}

	@MainActor
	public var companyInfoView : some View
	{
		CompanyInfoView(company: $ledger.company, baseReport: ledger.baseReport)
	}

	@MainActor
	public func inventoryView(baseAssets : Assets? = nil, baseDebts : Debts? = nil) -> some View
	{
		InventoryView(
			businessPeriod: ledger.period,
			assets: $ledger.assets,
			debts: $ledger.debts,
			baseAssets: baseAssets,
			baseDebts: baseDebts
		)
	}

	@MainActor
	public var baseReportView : some View
	{
		if let baseReport = ledger.baseReport {
			return AnyView (
				ScrollView {
					ReportView(report: baseReport)
						.padding()
				}
			)
		}
		return AnyView(EmptyView())
	}

	/// Dependency injection interface for the report generating function.
	public struct ReportGenerator
	{
		let generate : () async -> ()
		let clear : () -> ()
		public init(generate: @escaping () async -> Void, clear: @escaping () -> Void)
		{
			self.generate = generate
			self.clear = clear
		}
	}

	@MainActor
	public func reportGeneratorView(report : Report?, reportType: Binding<ReportType>, timestamp : Date?, generator : ReportGenerator) -> some View
	{
		ReportGeneratorView(report: report, reportType: reportType, reportYear: _ledger.wrappedValue.period.end.year, timestamp: timestamp, generator: generator)
	}

	@MainActor
	public static func reportView(report : Report) -> some View
	{
		ScrollView
		{
			ReportView(report: report)
				.padding()
		}
	}
}
