// Copyright 2022-2025 Kai Oezer

import SwiftUI
import Buchhaltung

struct DayEntryField : View
{
	@Binding var day : Day
	var period : Period?

	var body : some View
	{
		DatePicker(selection: _dayDate, in: period?.dateRange ?? Period.maxDateRange, displayedComponents: .date, label: {})
		#if os(macOS)
			.datePickerStyle(.field)
		#else
			.datePickerStyle(.compact)
		#endif
			.modifier(ISO8601DayFormatter())
			.labelsHidden()
	}

	private var _dayDate : Binding<Date>
	{
		Binding<Date>(
			get: { day.midday },
			set: { day = Day(date: $0) }
		)
	}

	/// provides workaround for making the date picker display dates in ISO8601 format
	private struct ISO8601DayFormatter: ViewModifier
	{
		func body(content: Content) -> some View
		{
			content
			.environment(\.locale, Locale(languageCode: .german, languageRegion: .sweden))
		}
	}
}

// MARK: - Preview

#Preview("DayEntryField")
{
	@Previewable @State var day : Day = .init(2023, 5, 20)!

	DayEntryField(day: $day)
		.padding(20)
}
