// Copyright 2022-2024 Kai Oezer

import SwiftUI
import Buchhaltung

struct MoneyText : View
{
	private let _money : Money
	private let _validator : MoneyAmountValidator?
	var foregroundColor : Color?
	@Environment(\.bhErrorColor) var errorColor : Color
	@Environment(\.colorScheme) var colorScheme : ColorScheme

	init(_ money : Money, amountValidator : MoneyAmountValidator? = nil)
	{
		_money = money
		_validator = amountValidator
	}

	var body : some View
	{
		Text(verbatim: "\(_money.bh) €")
			.bhMoney()
			.foregroundColor((_validator?(_money) ?? true) ? .none : errorColor)
	}
}

struct MoneyAmountValidator
{
	private let _validationFunc : (Money?) -> Bool

	static var nonnegative : MoneyAmountValidator {
		Self.init{ $0 != nil && $0! >= .zero }
	}

	init(_ validationFunc : @escaping (Money?) -> Bool)
	{
		_validationFunc = validationFunc
	}

	func callAsFunction(_ money : Money? = nil) -> Bool
	{
		_validationFunc(money)
	}
}
