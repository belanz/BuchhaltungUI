// Copyright 2022-2025 Kai Oezer

import SwiftUI
import Buchhaltung
import RegexBuilder

struct MoneyEntryField : View
{
	@Environment(\.bhErrorColor) var errorColor : Color
	@Binding var money : Money
	var amountValidator : MoneyAmountValidator?

	var body : some View
	{
		TextField(value: $money, format: MoneyEntryFieldFormatStyle(), label: {})
			.bhMoney()
			.foregroundColor((amountValidator?(money) ?? true) ? .none : errorColor)
	}
}

struct MoneyEntryFieldFormatStyle : ParseableFormatStyle
{
	var parseStrategy : Money.BHParseStrategy
	{
		Money.BHParseStrategy()
	}

	func format(_ money : Money) -> String
	{
		"\(money.bh) €"
	}
}

extension FormatStyle where Self == MoneyEntryFieldFormatStyle
{
	static var bhMoneyEntry : MoneyEntryFieldFormatStyle
	{
		MoneyEntryFieldFormatStyle()
	}
}

// MARK: - Preview

#Preview("MoneyEntryField", traits: .fixedLayout(width: 200, height: 40))
{
	@Previewable @State var money : Money = 4.5

	MoneyEntryField(money: $money)
		.padding(20)
}
