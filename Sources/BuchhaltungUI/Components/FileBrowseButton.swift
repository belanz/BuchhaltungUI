// Copyright 2023-2025 Kai Oezer

import SwiftUI
#if canImport(AppKit)
import AppKit
#elseif canImport(UIKit)
import UIKit
#endif
import UniformTypeIdentifiers

public struct FileBrowseButton : View
{
	let fileTypes : [UTType]
	let action : (URL?) -> Void

	public init(fileTypes : [UTType] = [], action : @escaping (URL?) -> Void)
	{
		self.fileTypes = fileTypes
		self.action = action
	}

	public var body : some View
	{
		Button {
		#if canImport(AppKit)
			let openPanel = NSOpenPanel()
			openPanel.canChooseDirectories = false
			openPanel.canChooseFiles = true
			openPanel.canCreateDirectories = false
			openPanel.allowsMultipleSelection = false
			openPanel.allowedContentTypes = fileTypes
			let result = openPanel.runModal()
			action(result == .OK ? openPanel.url : nil)
		#elseif canImport(UIKit)
			logger.debug("Loading base documents from files is not supported on iOS.")
		#endif
		} label: {
			Image(systemName: "folder")
		}
		.buttonStyle(.borderless)
	}
}

#Preview
{
	@Previewable @State var selectedFile : URL? = nil

	VStack(spacing: 20)
	{
		FileBrowseButton { selectedFile = $0 }
		Text(selectedFile?.absoluteString ?? "")
	}
	.padding()
	.frame(width: 400)
}
