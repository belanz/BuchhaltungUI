// Copyright 2020-2023 Kai Oezer

import SwiftUI

struct FlowView<Content : View> : View
{
	var _content : () -> Content

	init(@ViewBuilder content : @escaping () -> Content)
	{
		_content = content
	}

	var body : some View
	{
		InternalFlowLayout(horizontalSpacing: 6)
		{
			_content()
		}
	}

	/// Layout for two date entry fields plus a text item for displaying error messages.
	private struct InternalFlowLayout : Layout
	{
		private let _horizontalMargin : CGFloat
		private let _verticalMargin : CGFloat

		init(horizontalSpacing : CGFloat = 4, verticalSpacing : CGFloat = 10)
		{
			_horizontalMargin = horizontalSpacing
			_verticalMargin = verticalSpacing
		}

		// swiftlint:disable nesting
		struct Positions
		{
			var fromDate : CGRect
			var toDate : CGRect
			var message : CGRect
			var boundingBox : CGRect { fromDate.union(toDate).union(message) }
			mutating func translate(_ p : CGPoint)
			{
				fromDate.origin.x += p.x
				fromDate.origin.y += p.y
				toDate.origin.x += p.x
				toDate.origin.y += p.y
				message.origin.x += p.x
				message.origin.y += p.y
			}
		}
		// swiftlint:enable nesting

		func sizeThatFits(proposal: ProposedViewSize, subviews: Subviews, cache: inout Void) -> CGSize
		{
			guard let positions = _layout(for: proposal, of: subviews) else { return .zero }
			let availableRect = CGRect(origin: .zero, size: CGSize(width: proposal.width ?? 0, height: proposal.height ?? 0))
			return availableRect.union(positions.boundingBox).size
		}

		func placeSubviews(in bounds: CGRect, proposal: ProposedViewSize, subviews: Subviews, cache: inout Void)
		{
			guard var positions = _layout(for: proposal, of: subviews) else { return }
			positions.translate(bounds.origin)
			subviews[0].place(at: positions.fromDate.origin, proposal: .init(positions.fromDate.size))
			subviews[1].place(at: positions.toDate.origin, proposal: .init(positions.toDate.size))
			subviews[2].place(at: positions.message.origin, proposal: .init(positions.message.size))
		}

		private func _layout(for proposedSize : ProposedViewSize, of subviews : Subviews) -> Positions?
		{
			let preferredSizes = subviews.map{ $0.sizeThatFits(.unspecified) }
			guard preferredSizes.count == 3 else { return nil }
			let datesRowWidth = preferredSizes[0].width + _horizontalMargin + preferredSizes[1].width
			let singleRowWidth = datesRowWidth + _horizontalMargin + preferredSizes[2].width
			var positions = Positions(
				fromDate: CGRect(origin: .zero, size: preferredSizes[0]),
				toDate: CGRect(origin: CGPoint(x: preferredSizes[0].width + _horizontalMargin, y: 0), size: preferredSizes[1]),
				message: CGRect(origin: CGPoint(x: preferredSizes[0].width + preferredSizes[1].width + 2 * _horizontalMargin, y: 0), size: preferredSizes[2])
			)
			guard proposedSize != .infinity else { return positions }
			guard let proposedWidth = proposedSize.width else { return nil }
			if proposedWidth < singleRowWidth
			{
				let lineHeight = preferredSizes[0].height + _verticalMargin
				if proposedWidth >= datesRowWidth
				{
					positions.message.origin = CGPoint(x: 0, y: lineHeight)
				}
				else
				{
					positions.toDate.origin = CGPoint(x: 0, y: lineHeight)
					positions.message.origin = CGPoint(x: 0, y: 2 * lineHeight)
				}
			}
			return positions
		}
	}

}

