// Copyright 2023-2025 Kai Oezer

import SwiftUI

struct ValueTrackingOverlayButton : View
{
	let text : Text
	let topMargin : CGFloat
	let trailingMargin : CGFloat
	let action : () -> ()

	var body : some View
	{
		VStack
		{
			Spacer()
				.frame(height: topMargin)
			HStack
			{
				Spacer()
				text
					.foregroundColor(.accentColor)
					.padding(EdgeInsets(top: 8, leading: 10, bottom: 8, trailing: 10))
					.onTapGesture { action() }
					.background(.ultraThinMaterial)
					.clipShape(RoundedRectangle(cornerRadius: 8))
			}
			.padding([.trailing], trailingMargin)
			Spacer()
		}
	}
}

/// Adds an overlay button with the given text and the given margins.
///
/// Tapping (clicking) the button triggers the given `action`.
struct ValueTrackingOverlayButtonViewModifier<Value> : ViewModifier where Value : Equatable
{
	@Binding var value : Value
	let observer : (Value)->()
	let isVisible : Bool
	let text : Text
	let topMargin : CGFloat
	let trailingMargin : CGFloat
	let action : () -> ()

	func body(content: Content) -> some View
	{
		content
			.onChange(of: value, initial: true) { _, newValue in
				observer(newValue)
			}
			.overlay {
				if isVisible
				{
					ValueTrackingOverlayButton(text: text, topMargin: topMargin, trailingMargin: trailingMargin, action: action)
						.transition(.asymmetric(insertion: .push(from: .trailing), removal: .move(edge: .trailing)) )
						.transaction { transaction in
							transaction.animation = .spring(dampingFraction: 1.0, blendDuration: 1.0)
						}
				}
			}
	}
}

extension View
{
	func valueTrackingOverlayButton<Value : Equatable>(
		value : Binding<Value>,
		observer : @escaping (Value)->(),
		isVisible : Bool,
		text : Text,
		topMargin : CGFloat = 0,
		trailingMargin : CGFloat = 0,
		action : @escaping () -> ()
	) -> some View
	{
		self.modifier(ValueTrackingOverlayButtonViewModifier(
			value: value,
			observer: observer,
			isVisible: isVisible,
			text: text,
			topMargin: topMargin,
			trailingMargin : trailingMargin,
			action: action
		))
	}
}

// MARK: - Preview

#Preview("ValueTrackingOverlayButtonViewModifier", traits: .fixedLayout(width: 300, height: 140))
{
	@Previewable @State var shouldShowOverlayButton : Bool = true
	@Previewable @State var showsOverlayButton : Bool = false
	@Previewable @State var testColors : [Color] = []
	@Previewable @State var defaultColor : Color = .white
	@Previewable @State var colorIndex : Int = 0

	VStack
	{
		let color = testColors.isEmpty ? defaultColor : testColors[colorIndex % testColors.count]
		VStack
		{
			HStack
			{
				Toggle(isOn: $shouldShowOverlayButton) {
					Text(verbatim:"Show")
				}
				Spacer()
			}.frame(height: 28)
			ScrollView
			{
				VStack
				{
					Group
					{
						color.brightness(0.1)
						color.brightness(0.3)
						color.brightness(0.5)
						Color.clear
					}
					.frame(height: 50)
				}
			}
		}
		.overlay {
			if testColors.isEmpty {
				ProgressView()
					.controlSize(.large)
			}
		}
		.onAppear {
			Task {
				try await Task.sleep(for: .seconds(3))
				testColors = [.red, .yellow, .blue, .green, .init(white: 0.3)]
			}
		}
		.padding()
		.valueTrackingOverlayButton(
			value: $testColors,
			observer: { _ in },
			isVisible: shouldShowOverlayButton && !testColors.isEmpty,
			text: Text(verbatim: "Colorize"),
			topMargin: 60,
			trailingMargin: 30,
			action: { colorIndex += 1 })
	}
}
