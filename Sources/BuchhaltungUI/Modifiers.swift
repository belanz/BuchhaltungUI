// Copyright 2022-2023 Kai Oezer

import SwiftUI

struct BHTitleTextModifier : ViewModifier
{
	func body(content: Content) -> some View
	{
		content
			.font(.system(.headline))
			.foregroundColor(.accentColor)
	}
}

extension View
{
	func bhTitle() -> some View
	{
		modifier(BHTitleTextModifier())
	}
}

// MARK: -

struct BHSubtitleTextModifier : ViewModifier
{
	func body(content: Content) -> some View
	{
		content
			.font(.system(.body))
	}
}

extension View
{
	func bhSubtitle() -> some View
	{
		modifier(BHSubtitleTextModifier())
	}
}

// MARK: -

struct BHMoneyTextModifier : ViewModifier
{
	func body(content: Content) -> some View
	{
		content
			.font(.system(.body).monospaced())
			.multilineTextAlignment(.trailing)
			.labelsHidden()
	}
}

extension View
{
	func bhMoney() -> some View
	{
		modifier(BHMoneyTextModifier())
	}
}

// MARK: -

struct BHUnderlinedInputModifier : ViewModifier
{
	@Environment(\.bhErrorColor) var errorColor : Color
	@Environment(\.bhFocusedColor) var focusedColor : Color
	@Environment(\.colorScheme) var colorScheme : ColorScheme
	static let unfocusedColorLightMode = Color(white: 0.9)
	static let unfocusedColorDarkMode = Color(white: 0.2)

	let focus : FocusState<String?>.Binding
	let focusID : String
	let isValid : Bool

	func body(content: Content) -> some View
	{
		VStack
		{
			content
				.focused(focus, equals: focusID)
			Spacer()
				.frame(height: 2)
			Group
			{
				if !isValid {
					errorColor
				} else if self.focus.wrappedValue == focusID {
					focusedColor
				} else {
					colorScheme == .dark ? Self.unfocusedColorDarkMode : Self.unfocusedColorLightMode
				}
			}
			.frame(height: 1.0)
		}
		.transaction { $0.animation = nil }
	}
}

extension View
{
	func bhUnderlinedInput(focus : FocusState<String?>.Binding, focusID : String, isValid : Bool) -> some View
	{
		modifier(BHUnderlinedInputModifier(focus: focus, focusID: focusID, isValid: isValid))
	}
}

// MARK: -

public struct BHInsertRemoveItemTransitionModifier : ViewModifier
{
	public func body(content : Content) -> some View
	{
		content
			.transition(.asymmetric(
				insertion: .push(from: .top),
				removal: .move(edge: .top).combined(with: .opacity)
			))
	}
}

extension View
{
	func bhInsertRemoveItemTransition() -> some View
	{
		modifier(BHInsertRemoveItemTransitionModifier())
	}
}
