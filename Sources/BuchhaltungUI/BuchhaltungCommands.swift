// Copyright 2023 Kai Oezer

import SwiftUI

///  The main purpose of this observable class is to provide clients of the BuchhaltungUI library
///  an interface for detecting if certain actions are available and for triggering these actions.
///
///  A client would typically create keyboard shortcut commands for the provided actions, as shown below.
///  ```
///  import BuchhaltungUI
///
///	 struct Client : App
///	 {
///	   @FocusedObject var buchhaltungCommands : BuchhaltungCommands?
///
///	   var body : some Scene
///    {
///      WindowGroup{
///        ContentView()
///      }
///      .commands {
///        CommandMenu("Buchhaltung") {
///          Button {
///            buchhaltungCommands?.addJournalRecord()
///          } label {
///            Text("Add Record")
///          }
///          .keyboardShortcut("R", modifiers: .command)
///          .disabled(buchhaltungCommands?.addJournalRecord == nil)
///        }
///      }
///    }
///	 }
///
///  struct ContentView : View
///  {
///    @StateObject var BuchhaltungCommands = BuchhaltungCommands()
///
///    var body : some View
///    {
///      content
///        .environmentObject(buchhaltungCommands)
///        .focusedSceneObject(buchhaltungCommands)
///    }
///  }
///
///  ```
public class BuchhaltungCommands : ObservableObject
{
	public typealias Command = () -> ()

	/// Adds a new ``JournalRecord`` to the ``Journal`` instance used by ``BuchhaltungViews/journalView``.
	@Published public var addJournalRecord : Command?

	@Published public var duplicateJournalRecord : Command?

	public init() {}
}
