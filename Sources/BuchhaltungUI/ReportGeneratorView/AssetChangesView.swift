// Copyright 2022-2024 Kai Oezer

import SwiftUI
import Buchhaltung
import DEGAAP

struct AssetChangesView : View
{
	let assetChangesStatement : AssetChangesStatement
	var accountsChart : DEGAAPChartDescriptor?
	static let moneyMinWidth = CGFloat(80)

	var body : some View
	{
		Grid(alignment: .center, horizontalSpacing: 12, verticalSpacing: 8)
		{
			GridRow
			{
				Color.clear
					.gridCellUnsizedAxes(.horizontal)
				_changeTypeText("asset_changes.acquisition_costs")
				_changeTypeText("asset_changes.depreciations")
				_changeTypeText("asset_changes.book_value")
			}
			GridRow
			{
				Group
				{
					txt("asset_changes.asset")
						.gridCellAnchor(.leading)
					_changeDateText("asset_changes.period.begin")
					_changeDateText("asset_changes.period.end")
					_changeDateText("asset_changes.period.begin")
					_changeDateText("asset_changes.period.end")
					_changeDateText("asset_changes.period.current")
					_changeDateText("asset_changes.period.previous")
				}
				.opacity(0.5)
			}
			Color(white: 0.4)
				.frame(height: 1)
				.gridCellUnsizedAxes(.horizontal)
			ForEach(assetChangesStatement.changes.sorted{ $0.key < $1.key }, id: \.key)
			{
				let account = $0.key
				let changes = $0.value
				GridRow
				{
					AssetChangesAccountName(account: account, chart: accountsChart)
						.gridColumnAlignment(.leading)
					_moneyText(changes.acquisitionCosts.begin)
					_moneyText(changes.acquisitionCosts.end)
					_moneyText(changes.depreciations.begin)
					_moneyText(changes.depreciations.end)
					_moneyText(changes.bookValues.current)
					_moneyText(changes.bookValues.previous)
				}
			}
		}
	}

	private func _changeTypeText(_ loc : LocalizedStringKey) -> some View
	{
		txt(loc)
			.fixedSize()
			.gridCellColumns(2)
			.gridCellAnchor(.trailing)
			.gridCellUnsizedAxes(.horizontal)
	}

	private func _changeDateText(_ loc : String) -> some View
	{
		txt(LocalizedStringKey(loc))
			.fixedSize()
			.help(/TR(loc + ".help"))
			.gridColumnAlignment(.trailing)
	}

	private func _moneyText(_ value : Money) -> some View
	{
		MoneyText(value)
			.frame(minWidth: Self.moneyMinWidth, alignment: .trailing)
	}
}

fileprivate struct AssetChangesAccountName : View
{
	let account : AccountID
	let chart : DEGAAPChartDescriptor?
	@State var helpText : String = "…"
	@EnvironmentObject var degaap : ObservableDEGAAPEnvironment

	var body : some View
	{
		Text(account.description)
			.lineLimit(1)
			.fixedSize()
			.help(helpText)
			.task {
				if let chart {
					let applicationLanguage = Locale.preferredLanguages.isEmpty ? Locale.current.language : Locale.Language(identifier: Locale.preferredLanguages[0])
					let accountDescription = await degaap.degaap.description(for: account, chart: chart, language: applicationLanguage.languageCode ?? .german) ?? ""
					Task { @MainActor in
						helpText = accountDescription
					}
				}
			}
	}
}
