// Copyright 2022-2025 Kai Oezer

import SwiftUI
import Buchhaltung
import DEGAAP

struct ReportGeneratorView : View
{
	let report : Report?
	@Binding var reportType : ReportType
	let reportYear : Int
	@State var taxonomy : DEGAAPTaxonomyVersion = .allCases.last!
	let timestamp : Date?
	let generator : BuchhaltungViews.ReportGenerator?
	@State var isGeneratingReport : Bool = false
	@State var showsReportRemovalConfirmation : Bool = false
	@State var showsIssues : Bool = false
	@State var highlightsTimestamp : Bool = false
	@Environment(\.colorScheme) var colorScheme : ColorScheme
	@EnvironmentObject var issues : ObservableEmbeddableIssueCollection

	var body : some View
	{
		if let report {
			_reportView(report)
		} else {
			_emptyReportView
		}
	}

	private func _reportView(_ report : Report) -> some View
	{
		ScrollView
		{
			ZStack(alignment: .topLeading) {
				ReportView(report: report)
				_timestampView
			}
			.padding()
		}
		.safeAreaInset(edge: .top, spacing: 0) {
			VStack(spacing: 0) {
				_controlBar
				if showsIssues && !issues.isEmpty {
					Divider()
					IssuesView()
						.frame(height: 130)
						.transition(.asymmetric(insertion: .push(from: .top), removal: .push(from: .bottom)))
						.environmentObject(issues)
				}
			}
			.background(.thinMaterial)
		}
		.frame(maxWidth: .infinity, maxHeight: .infinity)
	}

	private var _emptyReportView : some View
	{
		GeometryReader { geo in
			VStack {
				_controlBar
					.background(.thinMaterial)
				Spacer()
				txt("report.generator.report_empty")
				Spacer()
					.frame(height: round(geo.size.height * 0.6))
			}
		}
	}

	private var _controlBar : some View
	{
		HStack(alignment: .firstTextBaseline, spacing: 20)
		{
			_reportTypePicker
			_taxonomyPicker
			Spacer()
			if report != nil {
				_deleteButton
			}
			_generateButton
		}
		.disabled(isGeneratingReport)
		.overlay {
			if isGeneratingReport {
				_waitIndicator
			} else if !issues.isEmpty {
				_issueIndicator
			}
		}
		.padding(.horizontal, 20)
		.frame(height: 40)
	}

	@ViewBuilder
	private var _timestampView : some View
	{
		if let timestamp {
			HStack(alignment: .firstTextBaseline, spacing: 8) {
				Text(timestamp.formatted(.iso8601.year().month().day().dateSeparator(.dash)))
					.bold().monospaced()
				Text(timestamp.formatted(.dateTime.hour(.twoDigits(amPM: .omitted)).minute(.twoDigits).second(.twoDigits)))
					.monospaced()
			}
			.foregroundColor(colorScheme == .dark ? .white : .black)
			.opacity(highlightsTimestamp ? 1.0 : 0.3)
			.help(txt("report.generator.timestamp.help"))
		}
	}

	private var _waitIndicator : some View
	{
		ViewThatFits(in: .horizontal)
		{
			HStack(spacing: 6)
			{
				ProgressView()
					.controlSize(.small)
				txt("report.generator.wait")
			}
			ProgressView()
				.controlSize(.small)
		}
	}

	private var _issueIndicator : some View
	{
		Toggle(isOn: $showsIssues.animation()) {
			Image(systemName: "exclamationmark.triangle")
				.symbolVariant(.fill)
				.symbolRenderingMode(.palette)
				.foregroundStyle(.black, .yellow)
		}
		.toggleStyle(.button)
		.help(txt("issues.indicator.help"))
	}

	private var _taxonomyPicker : some View
	{
		Picker(selection: _taxonomyFilter) {
			ForEach(DEGAAPTaxonomyVersion.allowedTaxonomies(businessYear: reportYear), id: \.self) { taxonomy in
				Text(/TR("report.generator.taxonomy.prefix") + " \(taxonomy.humanReadable)")
					.tag(taxonomy)
			}
		} label: {
		}
		.buttonStyle(.borderless)
		.labelsHidden()
		.fixedSize()
		.help(txt("report.generator.taxonomy.help"))
	}

	/// This filter makes sure that a valid taxonomy version for the reporting year is selected.
	private var _taxonomyFilter : Binding<DEGAAPTaxonomyVersion> {
		Binding<DEGAAPTaxonomyVersion>(
			get: {
				let allowedTaxonomies = DEGAAPTaxonomyVersion.allowedTaxonomies(businessYear: reportYear)
				precondition(!allowedTaxonomies.isEmpty)
				return allowedTaxonomies.contains(taxonomy) ? taxonomy : allowedTaxonomies.first!
			},
			set: { taxonomy = $0 }
		)
	}

	private var _reportTypePicker : some View
	{
		Picker(selection: $reportType) {
			ForEach(ReportType.allCases) { type in
				if let localizationKey = type.localizationKey {
					txt(LocalizedStringKey(localizationKey))
						.tag(type)
				}
			}
		} label: {
		}
		.buttonStyle(.borderless)
		.labelsHidden()
		.fixedSize()
		.help(txt("report.generator.report_type.help"))
	}

	private var _deleteButton : some View
	{
		BHItemRemoverButton {
			showsReportRemovalConfirmation = true
		}
		.help(txt("report.generator.remove.help"))
		.confirmationDialog(txt("report.generator.confirm_removal.title"), isPresented: $showsReportRemovalConfirmation) {
			Button(role: .destructive) {
				withAnimation {
					Task {
						generator?.clear()
					}
					showsReportRemovalConfirmation = false
				}
			} label: {
				txt("report.generator.confirm_removal.confirm")
			}
			Button(role: .cancel) {
				showsReportRemovalConfirmation = false
			} label: {
				txt("report.generator.confirm_removal.cancel")
			}
		} message: {
			txt("report.generator.confirm_removal.message")
		}
		.dialogSeverity(.critical)
	}

	private var _generateButton : some View
	{
		Button {
			Task {
				await _generateReport()
			}
		} label: {
			txt("report.generator.generate")
		}
		.help(txt("report.generator.generate.help"))
	}

	private func _generateReport() async
	{
		isGeneratingReport = true
		await generator?.generate()
		Task { @MainActor in
			isGeneratingReport = false
			_runTimestampBackgroundAnimation()
		}
	}

	private func _runTimestampBackgroundAnimation()
	{
		withAnimation(.easeIn(duration: 0.2)) {
			highlightsTimestamp = true
		}
		withAnimation(.easeOut(duration: 0.1).delay(0.3)) {
			highlightsTimestamp = false
		}
	}
}
