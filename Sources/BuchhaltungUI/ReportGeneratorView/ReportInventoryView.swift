// Copyright 2022 Kai Oezer

import SwiftUI
import Buchhaltung

struct ReportInventoryView : View
{
	let inventory : Inventory
	private let _debugHighlightCells = false
	@Environment(\.colorScheme) var colorScheme : ColorScheme

	var body : some View
	{
		Grid(alignment: .bottomLeading, horizontalSpacing: 8, verticalSpacing: 0)
		{
			// _expander
			_assets
			_emptyRow
			_debts
			_emptyRow
			_total
		}
		.padding([.bottom], 10)
	}

	@ViewBuilder
	private var _assets : some View
	{
		_categoryRow("A. \(TR("assets"))")
		_subcategoryRow("I. \(TR("assets.fixed"))", inventory.fixedAssetsTotal)
		ForEach(inventory.fixedAssetsValuation.sorted{ $0.key < $1.key }, id: \.key)
		{
			_assetRow(for: $0)
		}
		_subcategoryRow("II. \(TR("assets.current"))", inventory.currentAssetsTotal)
		ForEach(inventory.currentAssetsValuation.sorted{ $0.key < $1.key }, id: \.key)
		{
			_assetRow(for: $0)
		}
		_divider
		_categoryRow(/TR("assets.total"), trailingAmount: inventory.assetsTotal)
		_divider
	}

	@ViewBuilder
	private var _debts : some View
	{
		_categoryRow("B. \(TR("debts"))")
		_subcategoryRow("I. \(TR("debts.long-term"))", inventory.longTermDebtsValuation)
		_subcategoryRow("II. \(TR("debts.short-term"))", inventory.shortTermDebtsValuation)
		_divider
		_categoryRow(/TR("debts.total"), trailingAmount: inventory.debtTotal)
		_divider
	}

	@ViewBuilder
	private var _total : some View
	{
		_categoryRow("C. \(TR("inventory.total"))")
		_subcategoryRow("+ \(TR("assets.total"))", inventory.assetsTotal)
		_subcategoryRow("- \(TR("debts.total"))", inventory.debtTotal)
		_divider
		_categoryRow(/TR("inventory.total"), trailingAmount: inventory.total)
		_divider
	}

	private func _categoryRow(_ title : String, trailingAmount : Money? = nil) -> some View
	{
		GridRow
		{
			_textCell(title)
				.gridCellColumns(3)
			_expander
			_emptyCell()
			verticalLine
			if let trailingAmount
			{
				_moneyCell(trailingAmount)
			}
			else
			{
				_emptyCell(width: _moneyColumnWidth)
			}
		}
		.frame(height: _cellHeight)
	}

	private func _subcategoryRow(_ title : String, _ amount : Money) -> some View
	{
		GridRow
		{
			_indentation
			_textCell(title)
				.gridCellColumns(2)
			_expander
			_emptyCell()
			verticalLine
			_moneyCell(amount)
		}
		.frame(height: _cellHeight)
	}

	func _assetRow<T>(for assetItem : (T, Money)) -> some View
	{
		let _assetRowColor = Color(white: 0.4)

		return GridRow
		{
			_indentation
			_indentation
			_textCell(_title(for: assetItem.0), foregroundColor: _assetRowColor)
			_expander
			_moneyCell(assetItem.1)
			verticalLine
			_emptyCell(width: _moneyColumnWidth)
		}
		.frame(height: _cellHeight)
	}

	// swiftlint:disable cyclomatic_complexity
	func _title<T>(for assetType : T) -> String
	{
		if let fixedAssetType = assetType as? Assets.FixedAssetType
		{
			switch fixedAssetType
			{
				case .intangible: return /TR("assets.fixed.intangible.type.acquired")
				case .intangibleSelfMade: return /TR("assets.fixed.intangible.type.self_made")
				case .land: return /TR("assets.fixed.tangible.type.land")
				case .building: return /TR("assets.fixed.tangible.type.buildings")
				case .machinery: return /TR("assets.fixed.tangible.type.machinery")
				case .equipment: return /TR("assets.fixed.tangible.type.equipment.other")
				case .officeEquipment: return /TR("assets.fixed.tangible.type.equipment.office")
				case .factoryEquipment: return /TR("assets.fixed.tangible.type.equipment.factory")
				case .vehicle: return /TR("assets.fixed.tangible.type.vehicle.other")
				case .passengerVehicle: return /TR("assets.fixed.tangible.type.vehicle.passenger")
				case .commercialVehicle: return /TR("assets.fixed.tangible.type.vehicle.commercial")
				case .financial: return /TR("assets.fixed.tangible.type.financial")
				case .other: return /TR("assets.fixed.tangible.type.other")
			}
		}
		else if let currentAssetType = assetType as? Assets.CurrentAssetType
		{
			switch currentAssetType
			{
				case .material: return /TR("assets.current.type.material")
				case .inProgress: return /TR("assets.current.type.goods.in_progress")
				case .finishedGoods: return /TR("assets.current.type.goods.finished")
				case .receivable: return /TR("assets.current.type.receivable")
				case .security: return /TR("assets.current.type.securities")
				case .cashEquivalent: return /TR("assets.current.type.cash_equivalent")
				case .other: return /TR("assets.current.type.other")
			}
		}
		return ""
	}

	private let _cellHeight : CGFloat = 24.0
	private let _moneyColumnWidth : CGFloat = 100.0

	private var _indentation : some View
	{
		_emptyCell(width: 20)
	}

	private var _emptyRow : some View
	{
		GridRow
		{
			_emptyCell(columns: 3)
			_expander
			_emptyCell()
			verticalLine
			_emptyCell()
		}
	}

	private func _emptyCell(columns : UInt8 = 1, width : CGFloat = 1) -> some View
	{
		(_debugHighlightCells ? Color.gray : Color.clear)
			.frame(width: width)
			.gridCellColumns(Int(columns))
			.gridCellUnsizedAxes(.horizontal)
	}

	private func _textCell(_ text : String, foregroundColor : Color? = nil) -> some View
	{
		Text(text)
			.foregroundColor(foregroundColor ?? .none)
			.background(_debugHighlightCells ? Color.orange : Color.clear)
	}

	private func _moneyCell(_ amount : Money) -> some View
	{
		MoneyText(amount)
			.background(_debugHighlightCells ? Color.blue : Color.clear)
			.frame(width: _moneyColumnWidth, alignment: .trailing)
			.gridCellUnsizedAxes(.horizontal)
	}

	private var _expander : some View
	{
		Spacer()
			.frame(height:1)
	}

	private var _divider : some View
	{
		Divider()
			.gridCellUnsizedAxes(.horizontal)
	}

}
