// Copyright 2020-2025 Kai Oezer

import SwiftUI
import Buchhaltung
import DEGAAP

struct ReportView : View
{
	let report : Report
	@State var selectedSection : ReportSection = .all
	@State var accountsChart : DEGAAPChartDescriptor?
	@EnvironmentObject var degaap : ObservableDEGAAPEnvironment

	var body : some View
	{
		VStack(spacing: 20)
		{
			_periodAndType
			_sectionPicker
			_sectionViews
		}
		.textSelection(.enabled)
		.onAppear { _preventSelectionOfEmptySection() }
		.onChange(of: report, initial: true) { _, newReport in
			_preventSelectionOfEmptySection(for: newReport)
		}
	}

	private var _periodAndType : some View
	{
		HStack
		{
			if let locKey = report.type.localizationKey {
				txt(LocalizedStringKey(locKey))
			}
			Text(report.period.description)
				.font(.body)
				.fixedSize()
		}
		.opacity(0.5)
	}

	private var _sectionPicker : some View
	{
		Picker(selection: $selectedSection) {
			ForEach(_displayedSections) { sectionOption in
				txt(LocalizedStringKey(sectionOption.title))
					.tag(sectionOption)
					.help(/TR(sectionOption.description))
			}
		} label: {
		}
		.pickerStyle(.segmented)
		.labelsHidden()
		.fixedSize()
	}

	private var _sectionViews : some View
	{
		VStack(spacing: 50)
		{
			ForEach(_displayedSections.filter{$0 != .all}) { section in
				if (selectedSection == .all) || (selectedSection == section)
				{
					VStack(spacing: 20)
					{
						txt(LocalizedStringKey(section.description))
							.bold()
						_view(for: section)
					}
				}
			}
		}
		.frame(maxWidth: .infinity)
	}

	private var _displayedSections : [ReportSection]
	{
		ReportSection.allCases.filter{
			(($0 != .assetChanges) || (report.notes.assetChangesStatement != nil))
		}
	}

	@ViewBuilder
	private func _view(for section : ReportSection) -> some View
	{
		switch section {
			case .balanceSheet:
				BalanceSheetView(balanceSheet: report.balanceSheet)
			case .income:
				IncomeStatementView(incomeStatement: report.incomeStatement)
			case .inventory:
				ReportInventoryView(inventory: report.inventory)
			case .assetChanges:
				if let statement = report.notes.assetChangesStatement {
					AssetChangesView(assetChangesStatement: statement, accountsChart: accountsChart)
						.task {
							accountsChart = await _loadAccountsChart()
						}
				}
			default:
				EmptyView()
		}
	}

	private func _preventSelectionOfEmptySection(for report : Report? = nil)
	{
		if (selectedSection == .assetChanges) && (report?.notes.assetChangesStatement == nil) {
			selectedSection = .all
		}
	}

	private func _loadAccountsChart() async -> DEGAAPChartDescriptor?
	{
		let chart = DEGAAPChartDescriptor(taxonomy: report.taxonomy, fiscal: report.type == .fiscal)
		return await degaap.degaap.contains(chart: chart) ? chart : nil
	}
}
