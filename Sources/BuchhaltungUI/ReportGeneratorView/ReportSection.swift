// Copyright 2023 Kai Oezer

enum ReportSection : Int, Codable, CaseIterable
{
	case all = 0
	case balanceSheet
	case income
	case inventory
	case assetChanges
}

extension ReportSection : Identifiable
{
	var id : ReportSection.RawValue
	{
		self.rawValue
	}
}

extension ReportSection : CustomStringConvertible
{
	var title : String
	{
		switch self
		{
			case .all:          return "report.section.all"
			case .balanceSheet: return "report.section.balance"
			case .income:       return "report.section.income"
			case .inventory:    return "report.section.inventory"
			case .assetChanges: return "report.section.changes"
		}
	}

	var description : String
	{
		switch self
		{
			case .all:          return "report.section.all.help"
			case .balanceSheet: return "report.section.balance.help"
			case .income:       return "report.section.income.help"
			case .inventory:    return "report.section.inventory.help"
			case .assetChanges: return "report.section.changes.help"
		}
	}
}
