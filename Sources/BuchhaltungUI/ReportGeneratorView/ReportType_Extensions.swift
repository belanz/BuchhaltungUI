// Copyright 2023-2024 Kai Oezer

import Buchhaltung

extension ReportType : @retroactive Identifiable
{
	public var id : ReportType.RawValue { rawValue }
}

extension ReportType
{
	var localizationKey : String?
	{
		switch self
		{
			case .fiscal: return "report.type.fiscal"
			case .trade:  return "report.type.trade"
			default: return nil
		}
	}
}
