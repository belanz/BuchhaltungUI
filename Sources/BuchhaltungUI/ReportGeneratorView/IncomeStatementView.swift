// Copyright 2022-2023 Kai Oezer

import SwiftUI
import Buchhaltung

struct IncomeStatementView : View
{
	let incomeStatement : IncomeStatement

	var body : some View
	{
		LazyVGrid(
			columns: [
				GridItem(.fixed(20), alignment: .center),
				GridItem(.flexible(minimum: 100), alignment: .leading),
				GridItem(.fixed(100), alignment: .trailing),
				GridItem(.fixed(100), alignment: .trailing)],
			alignment: .center,
			spacing: 14,
			pinnedViews: [])
		{
			Group
			{
				Color.clear
				Color.clear
				txt("income_statement.current_period")
				txt("income_statement.previous_period")
			}

			_item("income_statement.earnings", \.earnings)
			_item("income_statement.otherEarnings", \.otherEarnings, prefix: "+")
			_item("income_statement.materialCosts", \.materialCosts, prefix: "-")
			_item("income_statement.salaries", \.salaries, prefix: "-")
			_item("income_statement.depreciations", \.depreciations.monetaryValue, prefix: "-")
			_item("income_statement.otherExpenses", \.otherExpenses, prefix: "-")
			_item("income_statement.tax", \.tax, prefix: "-")
			_profit_loss()
		}
	}

	@ViewBuilder
	private func _profit_loss() -> some View
	{
		Group
		{
			Color.clear
			Color.clear
			Divider()
			Divider()
		}

		_item("income_statement.profit_loss", \.profitLoss, prefix: "=")
	}

	@ViewBuilder
	private func _item(_ title : LocalizedStringKey, _ keypath : KeyPath<IncomeStatement.Results, Money>, prefix : String = "") -> some View
	{
		Text(prefix)
		txt(title)
		MoneyText(incomeStatement.results[keyPath: keypath])
		MoneyText(incomeStatement.previousYearResults[keyPath: keypath])
	}

}
