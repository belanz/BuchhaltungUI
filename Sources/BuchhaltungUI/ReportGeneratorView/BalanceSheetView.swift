// Copyright 2022-2023 Kai Oezer

import SwiftUI
import Buchhaltung

struct BalanceSheetView : View
{
	let balanceSheet : BalanceSheet

	var body : some View
	{
		HStack(alignment: .top, spacing: 0)
		{
			_assets
			verticalLine
			_equity_liabilities
		}
		.frame(maxWidth: .infinity, maxHeight: .infinity)
	}

	private var _assets : some View
	{
		_balanceSheetSection("balance_sheet.assets.title", total: balanceSheet.assets.sum) {
			_item("balance_sheet.assets.fixed", value: balanceSheet.assets.fixedAssets)
			_item("balance_sheet.assets.current", value: balanceSheet.assets.currentAssets)
			_item("balance_sheet.assets.deferrals", value: balanceSheet.assets.deferrals.monetaryValue)
			_item("balance_sheet.assets.deferred_tax", value: balanceSheet.assets.deferredTax.monetaryValue)
			_item("balance_sheet.assets.surplus", value: balanceSheet.assets.surplus.monetaryValue)
		}
	}

	private var _equity_liabilities : some View
	{
		_balanceSheetSection("balance_sheet.eqliab.title", total: balanceSheet.eqLiab.sum) {
			_item("balance_sheet.eqliab.equity", value: balanceSheet.eqLiab.equity)
			_item("balance_sheet.eqliab.provisions", value: balanceSheet.eqLiab.provisions)
			_item("balance_sheet.eqliab.liabilities", value: balanceSheet.eqLiab.liabilities)
			_item("balance_sheet.eqliab.deferrals", value: balanceSheet.eqLiab.deferrals.monetaryValue)
			_item("balance_sheet.eqliab.deferred_tax", value: balanceSheet.eqLiab.deferredTax.monetaryValue)
		}
	}

	private func _balanceSheetSection<Content : View>(
		_ title : LocalizedStringKey,
		total : Money,
		@ViewBuilder _ content : () -> Content
	) -> some View
	{
		VStack
		{
			txt(title)
			horizontalLine
			VStack(spacing: 12)
			{
				content()
				_total(value: total)
					.padding([.top], 14)
			}
			.padding([.leading, .trailing], 10)
		}
	}

	private func _item(_ title : LocalizedStringKey, value : Money = .zero) -> some View
	{
		HStack
		{
			txt(title)
				.lineLimit(1)
				.font(.body)
				.frame(maxWidth: .infinity, alignment: .leading)
			MoneyText(value)
				.font(.body)
				.fixedSize()
		}
	}

	private func _total(value : Money = .zero) -> some View
	{
		HStack
		{
			Spacer()
			MoneyText(value)
				.font(.body)
				.fixedSize()
				.frame(alignment: .trailing)
		}
	}

}
