// Copyright 2023-2025 Kai Oezer

import SwiftUI
import IssueCollection
import Buchhaltung

struct IssuesView : View
{
	@EnvironmentObject var issues : ObservableEmbeddableIssueCollection
	@Environment(\.bhErrorColor) var errorColor : Color

	var body: some View
	{
		VStack(spacing: 10)
		{
			List(issues.embeddableIssues.issues) { issue in
				VStack(alignment: .leading) {
					_metadataLine(for: issue)
					_descriptionTextLine(for: issue)
				}
			}
			.scrollContentBackground(.hidden)
			_clearButton
		}
		.padding([.bottom], 8)
	}

	private func _metadataLine(for issue : Issue) -> some View
	{
		HStack {
			Text(verbatim: "\(issue.domain.description).\(issue.code.code)")
				.fixedSize()
			if let day = issue[.day] {
				Text(day.description)
					.fixedSize()
					.foregroundColor(errorColor)
			}
			if let account = issue[.accountID] {
				Text(account.description)
					.foregroundColor(errorColor)
			}
		}
		.opacity(0.8)
	}

	@ViewBuilder
	private func _descriptionTextLine(for issue : Issue) -> some View
	{
		let localizationKey = "issue.\(issue.domain).\(issue.code)"
		let localizedDescription = /TR(localizationKey)
		if localizedDescription == localizationKey {
			if let message = issue[.message] {
				Text(message)
			}
		} else {
			Text(localizedDescription)
		}
	}

	private var _clearButton : some View
	{
		HStack {
			Spacer()
			BHItemRemoverButton {
				withAnimation {
					issues.clear()
				}
			}
			.padding(4)
			.help(txt("issues.clear.help"))
			Spacer()
		}
	}
}

// MARK: - Preview

#Preview("IssuesView", traits: .fixedLayout(width: 500, height: 200))
{
	var testIssues : ObservableEmbeddableIssueCollection {
		var collection = IssueCollection()
		collection.append(Issue(domain: .ledger, code: .journalFixedAssetAccountNotSupported, metadata: [.day : Day(2023, 7, 13)!.bh, .accountID: DEGAAPAccounts.ass_fixed_equipment_other.description]))
		return ObservableEmbeddableIssueCollection(issues: EmbeddableIssueCollection(issues: collection))
	}

	IssuesView()
		.environmentObject(testIssues)
}
