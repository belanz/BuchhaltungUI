// Copyright 2022-2023 Kai Oezer

import XCTest
@testable import BuchhaltungUI

final class AnnotatedTextTests : XCTestCase
{
	func testTextOnly()
	{
		let basicText : AnnotationTextField.AnnotatedText = "Hello"
		XCTAssertEqual(basicText.text, "Hello")
		XCTAssertTrue(basicText.annotation.isEmpty)

		let basicTextWithSpaces : AnnotationTextField.AnnotatedText = " Hello  "
		XCTAssertEqual(basicTextWithSpaces.text, "Hello")
		XCTAssertTrue(basicTextWithSpaces.annotation.isEmpty)
	}

	func testAnnotationOnly()
	{
		let annotationOnly : AnnotationTextField.AnnotatedText = "(some annotation)"
		XCTAssertTrue(annotationOnly.text.isEmpty)
		XCTAssertTrue(annotationOnly.annotation.isEmpty)
	}

	func testCanonicalForm()
	{
		let annotatedText1 : AnnotationTextField.AnnotatedText = "Hello (World)"
		XCTAssertEqual(annotatedText1.text, "Hello")
		XCTAssertEqual(annotatedText1.annotation, "World")

		let annotatedText2 = AnnotationTextField.AnnotatedText("some_account_id (and its _short_ description)")
		XCTAssertEqual(annotatedText2.text, "some_account_id")
		XCTAssertEqual(annotatedText2.annotation, "and its _short_ description")
	}

	func testTrimming()
	{
		let annotatedText : AnnotationTextField.AnnotatedText = "   some_account_id ( and its _short_ description  ) "
		XCTAssertEqual(annotatedText.text, "some_account_id")
		XCTAssertEqual(annotatedText.annotation, "and its _short_ description")
	}

	func testAnnotationWithNestedBrackets()
	{
		let annotatedText : AnnotationTextField.AnnotatedText = "   some_account_id ( and its (quite detailed) description  ) "
		XCTAssertEqual(annotatedText.text, "some_account_id")
		XCTAssertEqual(annotatedText.annotation, "and its (quite detailed) description")
	}

	func testDEGAAPItem()
	{
		let annotatedText : AnnotationTextField.AnnotatedText = "bs.ass.currAss.cashEquiv.bank (Kassenbestand, Bundesbankguthaben, Guthaben bei Kreditinstituten und Schecks; Guthaben bei Kreditinstituten)"
		XCTAssertEqual(annotatedText.text, "bs.ass.currAss.cashEquiv.bank")
		XCTAssertEqual(annotatedText.annotation, "Kassenbestand, Bundesbankguthaben, Guthaben bei Kreditinstituten und Schecks; Guthaben bei Kreditinstituten")
	}

}
